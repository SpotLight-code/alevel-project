package com.alevel.java.nix.homework8.aggregation;

public class Iron extends BaseSubstance{

    private final static double toVigor = 1500;
    private final static double toGas = 2800;
    private final static String name = "Iron";

    @Override
    double getVigorTemperature() {
        return toVigor;
    }

    @Override
    double getGasTemperature() {
        return toGas;
    }

    @Override
    public String getName() {
        return name;
    }
}
