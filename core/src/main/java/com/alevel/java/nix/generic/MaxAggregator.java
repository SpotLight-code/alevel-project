package com.alevel.java.nix.generic;

import java.util.Optional;

public class MaxAggregator<T extends Comparable<T>> implements Aggregator<T, Optional<T>> {

    @Override
    public Optional<T> aggregate(T[] items) {
        if (items.length == 0) {
            return Optional.empty();
        }

        T max = items[0];

        for (int i = 1, len = items.length; i < len; i++) {
            if (max.compareTo(items[i]) < 0) {
                max = items[i];
            }
        }

        return Optional.of(max);
    }
}
