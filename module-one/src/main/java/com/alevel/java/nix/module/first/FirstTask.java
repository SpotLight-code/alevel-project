package com.alevel.java.nix.module.first;

import java.util.HashSet;
import java.util.Set;

public class FirstTask {

    public static void main(String[] args) {

    }

    public int countUniqueElements(int... arr) {
        if (arr.length == 0) {
            return 0;
        }

        Set<Integer> integers = new HashSet<>();

        for (int value : arr) {
            integers.add(value);
        }

        return integers.size();
    }

    public boolean canHorseMove(int[] currentPos, int[] movePosition) {

        int[] moveLen = {Math.abs(movePosition[0] - currentPos[0]), Math.abs(movePosition[1] - currentPos[1])};

        if (moveLen[0] == 2 && moveLen[1] == 1) {
            return true;
        }

        return moveLen[1] == 2 && moveLen[0] == 1;
    }

    public double triangleSquare(int[] a, int[] b, int[] c) {

        int firstSum = a[0] * b[1] + b[0] * c[1] + c[0] * a[1];
        int secondSum = a[1] * b[0] + b[1] * c[0] + c[1] * a[0];

        return Math.abs((firstSum - secondSum) / 2.0);
    }

}
