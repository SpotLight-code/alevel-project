package com.alevel.java.nix.module.third;



public class ThirdTask {
    private boolean[][] desk;

    ThirdTask(boolean[][] desk) {
        this.desk = desk;
    }

    public void nextRound() {
        int m = desk.length;
        int n = desk[0].length;
        boolean[][] current = new boolean[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                current[i][j] = isKeepAlive(i, j);
            }
        }
        desk = current;

    }

    private boolean isKeepAlive(int ii, int jj) {
        int count = 0;
        for (int i = ii - 1; i <= ii + 1; i++) {
            for (int j = jj - 1; j <= jj + 1; j++) {
                try {
                    if ((j != jj || i != ii) && desk[i][j]) {
                        count++;
                    }
                } catch (Exception ignored) {
                }
            }
        }
        if (desk[ii][jj]) {
            return count == 2 || count == 3;
        } else {
            return count == 3;
        }

    }

    public boolean[][] getDesk() {
        return desk;
    }

    public void setDesk(boolean[][] desk) {
        this.desk = desk;
    }
}
