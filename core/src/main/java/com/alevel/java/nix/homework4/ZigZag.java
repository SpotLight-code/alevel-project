package com.alevel.java.nix.homework4;


public class ZigZag {

    public String convert(String s, int numRows) {

        int length = s.length();

        if (numRows <= 1 || length <= numRows) {
            return s;
        }
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < numRows; i++) {
            for (int j = i, k = 0; k <= length / numRows; j += numRows * 2 - 2, k++) {

                if (j != i && i != 0 && i != numRows - 1 && j - i * 2 < length) {
                    stringBuilder.append(s.charAt(j - i * 2));
                }

                if (j < length) {
                    stringBuilder.append(s.charAt(j));
                }
            }
        }
        return stringBuilder.toString();
    }
}
