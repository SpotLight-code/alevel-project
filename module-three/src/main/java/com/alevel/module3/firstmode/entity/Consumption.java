package com.alevel.module3.firstmode.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "consumption")
public class Consumption {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String inform;

    @ManyToMany()
    @JoinTable(
            name = "operation_consumption",
            joinColumns = @JoinColumn(name = "consumption_id"),
            inverseJoinColumns = @JoinColumn(name = "operation_id")
    )
    private final List<Operation> operations = new ArrayList<>();

    public Consumption() {
    }

    public Consumption(String inform) {
        this.inform = inform;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInform() {
        return inform;
    }

    public void setInform(String inform) {
        this.inform = inform;
    }

    public List<Operation> getOperations() {
        return operations;
    }

    public void addOperation(Operation operation) {
        operations.add(operation);
    }

    @Override
    public String toString() {
        return inform;
    }
}
