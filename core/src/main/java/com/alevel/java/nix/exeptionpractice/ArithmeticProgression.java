package com.alevel.java.nix.exeptionpractice;

public class ArithmeticProgression {
    private final int initial;
    private final int step;


    public ArithmeticProgression(int initial, int step) throws ProgressionConfigurationException {
        if (step == 0) {
            throw new ProgressionConfigurationException("Step can't be 0!");
        }

        this.initial = initial;
        this.step = step;
    }

    public int calculate(int n) throws ProgressionConfigurationException {
        if (n < 0) {
            throw new ProgressionConfigurationException("Step can't be less than 1");
        }

        return initial + step*n;
    }

}
