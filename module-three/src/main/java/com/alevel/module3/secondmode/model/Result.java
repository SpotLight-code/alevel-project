package com.alevel.module3.secondmode.model;

import java.text.MessageFormat;

public class Result {
    private final String date;

    private final int cost;
    private final int sum;
    private final int balance;

    public Result(String date, int cost, int sum, int balance) {
        this.date = date;
        this.cost = cost;
        if (cost > 0) {
            this.sum = sum + cost;
        } else {
            this.sum = sum;
        }
        this.balance = balance + cost;
    }

    public int getSum() {
        return sum;
    }

    public int getBalance() {
        return balance;
    }

    public int getCost() {
        return cost;
    }

    @Override
    public String toString() {
        return MessageFormat.format("{0},{1},{2},{3},{4}", date, cost, sum, balance, System.lineSeparator());
    }
}
