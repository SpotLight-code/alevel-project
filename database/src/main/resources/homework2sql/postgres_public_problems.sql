create table problems
(
    id     integer not null
        constraint problems_pkey
            primary key,
    "from" integer not null
        constraint problems_from_fkey
            references city,
    "to"   integer not null
        constraint problems_to_fkey
            references city
);


INSERT INTO public.problems (id, from_id, to_id) VALUES (0, 1, 4);
INSERT INTO public.problems (id, from_id, to_id) VALUES (1, 2, 4);
INSERT INTO public.problems (id, from_id, to_id) VALUES (4, 2, 5);
INSERT INTO public.problems (id, from_id, to_id) VALUES (5, 5, 2);
INSERT INTO public.problems (id, from_id, to_id) VALUES (2, 2, 3);