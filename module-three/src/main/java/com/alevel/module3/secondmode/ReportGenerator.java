package com.alevel.module3.secondmode;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Instant;

public class ReportGenerator {
    private final DatabaseManip databaseManip;
    private final static String PATTERN = "date,cost,total Income,balance,\n";
    private final String path;

    public ReportGenerator(String userName, String password, String email, Instant before, Instant finish, String path) {
        databaseManip = new DatabaseManip(userName, password, email, before, finish);
        this.path = path;
    }

    public void generateCSV() {
        try (FileWriter fileWriter = new FileWriter(new File(path))) {
            fileWriter.write(PATTERN);
            for (var res : databaseManip.getResults()) {
                fileWriter.write(res.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public DatabaseManip getDatabaseManip() {
        return databaseManip;
    }
}
