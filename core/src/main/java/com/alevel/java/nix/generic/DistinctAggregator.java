package com.alevel.java.nix.generic;

import java.util.Arrays;
import java.util.HashSet;

public class DistinctAggregator<T> implements Aggregator<T, Integer> {
    @Override
    public Integer aggregate(T[] items) {
        return new HashSet<>(Arrays.asList(items)).size();
    }
}
