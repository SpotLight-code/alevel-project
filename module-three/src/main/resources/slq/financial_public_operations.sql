create table operations
(
    id         serial  not null
        constraint operations_pkey
            primary key,
    date       timestamp,
    account_id integer
        constraint operations_account_id_fkey
            references accounts
            on update cascade on delete cascade,
    cost       integer not null
);

alter table operations
    owner to postgres;

