package com.alevel.java.nix.homework6.model;

import ch.qos.logback.classic.Logger;

import org.slf4j.LoggerFactory;

import java.time.ZonedDateTime;


public class TicTacToe3_3 implements TicTacToe {
    private static final Logger log = (Logger) LoggerFactory.getLogger(TicTacToe3_3.class);
    private final int SIZE = 13;
    private final int[] MOVES_CASHED = {16, 21, 26, 76, 81, 86, 136, 141, 146};
    private char[][] desk = new char[SIZE-2][SIZE];
    private int iteration;
    private final char[] SYMBOLS = {'X', 'O'};
    private MoveStatus moveStatus;

    private void initEmptyDesk() {
        moveStatus.setGameFinished(false);
        iteration = 0;
        char[][] str = {
                {' ', ' ', ' ', '|', '|', ' ', ' ', ' ', '|', '|', ' ', ' ', ' '},
                {'=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '=', '='}};

        for (int i = 1; i <= SIZE-2; i++) {
            if (i % 4 == 0) {
                System.arraycopy(str[1], 0, desk[i - 1], 0, SIZE);
            } else {
                System.arraycopy(str[0], 0, desk[i - 1], 0, SIZE);
            }
        }
        log.info("Set up desk empty");
    }

    public TicTacToe3_3() {
        moveStatus = new MoveStatus();
        initEmptyDesk();
    }

    public char whoNowMove() {
        return SYMBOLS[iteration % 2];
    }

    public MoveStatus move(int place) throws Exception {

        if (place > 9 || place < 1) {
            log.warn("Warning, place given by {} is invalid. Number {} must be 0 < ... < 10",
                    SYMBOLS[iteration % 2], place);
            throw new Exception("Error! Number must be 0 < ... < 10");
        }

        if (!isSymbolIsSameWithSymbolInDesk(place, ' ')) {
            log.warn("Warning, place {} which {} try to use is already in use", place, SYMBOLS[iteration % 2]);
            throw new Exception("Error! This point is already in use");
        }

        setSymbol(place, SYMBOLS[iteration % 2]);
        log.info("{} moved to {}", SYMBOLS[iteration % 2], place);

        if (iteration >= 4) {
            if (checkWinner(place)) {
                log.info("Winner found! {} - win", SYMBOLS[iteration % 2]);
                moveStatus.setWinner(SYMBOLS[iteration % 2]);
                moveStatus.setGameFinished(true);

            } else if (iteration == 8) {
                log.info("Draw was happened! Game will be restart.");
                moveStatus.setDraw(true);
                moveStatus.setGameFinished(true);
            }

        }

        iteration++;
        return moveStatus;
    }

    public void reset() {
        initEmptyDesk();
    }

    private int getI(int pos) {
        return MOVES_CASHED[pos - 1] / 0XF;
    }

    private int getJ(int pos) {
        return MOVES_CASHED[pos - 1] % 0XF;
    }

    private void setSymbol(int pos, char symbol) {
        desk[getI(pos)][getJ(pos)] = symbol;
    }

    public boolean isSymbolIsSameWithSymbolInDesk(int pos, char symbol) {
        return desk[getI(pos)][getJ(pos)] == symbol;
    }

    private boolean checkWinner(int move) {
        int lenRow = 3;
        char currentMoveSymbol = SYMBOLS[(iteration) % 2];
        boolean[] isWin = {true, true};

        int leftPosition = move > lenRow ? ((move - 1) / lenRow) * lenRow + 1: 1;
        int topPosition = move > lenRow ? (move - 1) % lenRow + 1 : move;

        for (int i = 0; i < lenRow && (isWin[0] || isWin[1]); i++) {
            isWin[0] = isWin[0] && isSymbolIsSameWithSymbolInDesk(leftPosition + i, currentMoveSymbol);
            isWin[1] = isWin[1] && isSymbolIsSameWithSymbolInDesk(topPosition + i * lenRow, currentMoveSymbol);
        }

        if (isWin[0] || isWin[1]) {
            return true;
        }

        if (move % 2 != 1) {
            return false;
        }

        if (Math.abs(move - 5) == 0 || Math.abs(move - 5) == 4) {
            isWin[0] = true;
            for (int i = 1; i <= 9 && isWin[0]; i += 4) {
                isWin[0] = isSymbolIsSameWithSymbolInDesk(i, currentMoveSymbol);
            }
        }
        if (Math.abs(move - 5) == 0 || Math.abs(move - 5) == 2) {
            isWin[1] = true;
            for (int i = 3; i <= 7 && isWin[1]; i += 2) {
                isWin[1] = isSymbolIsSameWithSymbolInDesk(i, currentMoveSymbol);
            }
        }
        return isWin[0] || isWin[1];
    }


    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (char[] des : desk) {
            stringBuilder.append(des).append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }
}
