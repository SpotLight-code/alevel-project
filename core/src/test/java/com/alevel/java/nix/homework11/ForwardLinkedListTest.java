package com.alevel.java.nix.homework11;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ForwardLinkedListTest {

    ForwardLinkedList<String> list;

    @BeforeEach
    void init() {
        list = new ForwardLinkedList<>();
    }

    @Test
    void add() {
        assertEquals(0, list.size());
        assertTrue(list.isEmpty());

        list.add("test");

        assertTrue(list.contains("test"));
        assertEquals(0, list.indexOf("test"));
        assertEquals(1, list.size());
        assertFalse(list.isEmpty());
        assertEquals("test", list.get(list.indexOf("test")));
    }

    @Test
    void set() {
        list.add("test");

        assertFalse(list.isEmpty());
        String str = "New Text";
        String old = list.get(0);

        assertEquals(old, list.set(0, str));
        assertEquals(str, list.get(0));
    }

    @Test
    void testAdd() {
        list.add("New Text");

        list.add("3");
        list.add(1, "2");

        assertEquals("New Text", list.get(0));
        assertEquals("2", list.get(1));
        assertEquals("3", list.get(2));

    }

    @Test
    void remove() {
        list.add("New Text");
        list.add(Integer.MAX_VALUE, "2");
        list.add(Integer.MAX_VALUE, "3");

        assertEquals(3, list.size());
        assertTrue(list.contains("New Text"));
        assertTrue(list.remove("New Text"));
        assertFalse(list.remove("New Text"));
        assertEquals(2, list.size());
        assertFalse(list.contains("New Text"));

        assertEquals("2", list.get(0));
        assertEquals("2", list.remove(0));
        assertEquals(1, list.size());
        assertEquals("3", list.get(0));
    }


    @Test
    void clear() {
        list.add(Integer.MAX_VALUE, "2");
        assertEquals(1, list.size());
        for (int i = 0; i < 10; i++) {
            list.add(Integer.toString(i));
        }
        assertEquals(11, list.size());
        list.clear();
        assertEquals(0, list.size());
        assertTrue(list.isEmpty());
    }
}