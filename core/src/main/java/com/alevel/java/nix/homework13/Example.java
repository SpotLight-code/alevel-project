package com.alevel.java.nix.homework13;

import java.util.Objects;

public class Example {
    @CSVName(key = "name")
    private String name;

    @CSVName(key = "age")
    private int age;

    public Example(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Example() {
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Example{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Example example = (Example) o;
        return age == example.age &&
                Objects.equals(name, example.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}
