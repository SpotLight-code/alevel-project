package com.alevel.java.nix.practice1.gallows;

public final class Status {
    private String currentWord;
    private boolean isGameFinished = false;
    private int chances;
    private final String correctWord;

    public Status(String currentWord, int chances, String correctWord) {
        this.correctWord = correctWord;
        this.currentWord = currentWord;
        this.chances = chances;
    }

    public void setGameFinished() {
        isGameFinished = true;
    }

    public int getChances() {
        return chances;
    }

    void setCurrentWord(String string){
        currentWord = string;
        if(currentWord.equals(correctWord)){
            isGameFinished = true;
        }
    }

    public String getCurrentWord() {
        return currentWord;
    }

    public boolean isGameFinished() {
        return isGameFinished;
    }

    public String getCorrectWord(){
        if(!isGameFinished){
            throw new RuntimeException("Game not finished");
        }
        return correctWord;
    }

    public void decrementChances(){
        chances--;
        if(chances == 0){
            isGameFinished = true;
        }
    }
}
