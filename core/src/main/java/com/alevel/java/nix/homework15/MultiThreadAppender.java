package com.alevel.java.nix.homework15;

import java.io.FileWriter;
import java.io.IOException;

public class MultiThreadAppender {

    private volatile String input = "";

    public MultiThreadAppender() {
        new Thread(() -> {
            String input = this.input;

            try {
                while (true) {
                    Thread.sleep(1000);
                    if (input.length() != this.input.length()) {
                        try (FileWriter fw = new FileWriter("output.txt", false)) {
                            if (this.input.endsWith("quit")) {
                                fw.write(this.input.substring(0, this.input.length() - 4));
                                break;
                            } else {
                                input = this.input;
                                fw.write(input);
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }).start();
    }

    public String getInput() {
        return input;
    }

    public void append(String input) {
        this.input += input;
    }
}
