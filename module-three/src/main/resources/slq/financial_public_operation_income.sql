create table operation_income
(
    operation_id integer
        constraint account_income_operation_id_fkey
            references operations,
    income_id    integer
        constraint account_income_income_id_fkey
            references income
);

alter table operation_income
    owner to postgres;

