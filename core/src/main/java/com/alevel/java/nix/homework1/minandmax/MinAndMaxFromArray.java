package com.alevel.java.nix.homework1.minandmax;

public class MinAndMaxFromArray {

    private int max = 0;
    private int min = 0;

    public int getMax() {
        return max;
    }

    public int getMin() {
        return min;
    }

    public void setUpMinAndMaxFromArray(int[] array) {

        if (array.length == 0) {
            min = max = 0;
        } else {
            min = max = array[0];
        }

        for (int value : array) {
            if (min > value) {
                min = value;
            }
            if (max < value) {
                max = value;
            }
        }
    }
}
