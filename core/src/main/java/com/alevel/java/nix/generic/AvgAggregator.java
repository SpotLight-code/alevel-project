package com.alevel.java.nix.generic;

public class AvgAggregator<T extends Number> implements Aggregator<T,Double> {

    @Override
    public Double aggregate(T[] items) {
        if (items.length == 0) {
            return 0.0;
        }
        double doubleResult = 0;
        long longResult = 0;


        for (T item : items) {
            if (item instanceof Float || item instanceof Double) {
                doubleResult += item.doubleValue();

            } else {
                longResult += item.longValue();

            }
        }

        return longResult == 0 ? doubleResult / items.length : (double) longResult / items.length;
    }
}
