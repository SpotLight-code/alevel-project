package com.alevel.java.nix.homework10;

@FunctionalInterface
public interface Block <T>{
    T run() throws Exception;
}