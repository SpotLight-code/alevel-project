package com.alevel.java.nix.homework12;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static java.nio.file.Paths.get;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.Objects.requireNonNull;

public class DirWizard {

    public static void copyDirectory(String path) throws IOException {
        recursiveDirCopy(path.substring(path.lastIndexOf('/') + 1), path);
    }

    private static void recursiveDirCopy(String current, String innerPath) throws IOException {
        new File(current).mkdir();
        for (File f : requireNonNull(new File(innerPath).listFiles())) {
            String nextFile = current + "/" + f.getName();
            if (f.isDirectory()) {
                recursiveDirCopy(nextFile, f.getPath());
            } else {
                Files.copy(get(f.getPath()), get(nextFile), REPLACE_EXISTING);
            }
        }
    }

}
