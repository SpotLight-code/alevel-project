package com.alevel.java.nix.filestream;

import org.junit.jupiter.api.Test;

import java.io.*;

import static com.alevel.java.nix.filestream.FileFilter.printLinesWhichContainSubStr;
import static org.junit.jupiter.api.Assertions.*;

class FileFilterTest {


    @Test
    void printLinesWhichContainSubStrTest() throws IOException {
        File file = new File("test.txt");

        assertDoesNotThrow(() -> assertTrue(file.createNewFile()));
        FileWriter fw = new FileWriter(file);

        fw.write("123\n");        //correct
        fw.write("1221233\n");    //correct
        fw.write("1223\n");       //incorrect
        fw.write("41424344\n");   //incorrect
        fw.write("1123");         //correct

        fw.close();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        PrintStream printStream = new PrintStream(byteArrayOutputStream);

        System.setOut(printStream);

        printLinesWhichContainSubStr("test.txt", "123");
        assertTrue(file.delete());

        assertEquals("123\n1221233\n1123\n", byteArrayOutputStream.toString());

    }



}