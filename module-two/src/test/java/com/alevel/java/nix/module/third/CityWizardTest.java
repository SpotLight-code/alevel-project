package com.alevel.java.nix.module.third;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CityWizardTest {

    @Test
    void testDoingTask() {
        assertDoesNotThrow(() -> {
            CityWizard cityWizard = CityWizard.init("src/test/resources/test.txt");
            int[] result = cityWizard.findAllTasks();
            assertEquals(3, result[0]);
            assertEquals(2, result[1]);
            assertEquals(2, result.length);
        });
    }

}