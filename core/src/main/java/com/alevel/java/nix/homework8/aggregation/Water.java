package com.alevel.java.nix.homework8.aggregation;

public class Water extends BaseSubstance{
    private final static double toVigor = 0;
    private final static double toGas = 100;
    private final static String name = "Water";

    @Override
    double getVigorTemperature() {
        return toVigor;
    }

    @Override
    double getGasTemperature() {
        return toGas;
    }

    @Override
    public String getName() {
        return name;
    }
}
