create table City(
  id int primary key not null,
  name varchar(100) unique not null
);

create table Connection
(
    cost   int not null ,
    'from' int references City (id) not null,
    'to'   int references City (id) not null
);

create table Problems(
    id int primary key not null,
    "from" int references City (id) not null,
    "to"   int references City (id) not null
);

create table FoundRoutes(
    problemId int references Problems(id),
    cost int
);

create table ImpossibleRoutes (problemId int references Problems(id));
