create table income
(
    id     serial       not null
        constraint income_pkey
            primary key,
    inform varchar(100) not null
        constraint income_inform_key
            unique
);

alter table income
    owner to postgres;

INSERT INTO public.income (id, inform) VALUES (1, 'salary');