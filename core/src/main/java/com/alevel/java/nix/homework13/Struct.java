package com.alevel.java.nix.homework13;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.*;

public class Struct {
    private final List<String[]> data;
    private final Map<String, Integer> headers;

    private Struct(final List<String[]> list, final Map<String, Integer> headers) {
        this.data = list;
        this.headers = headers;
    }

    public static Struct load(final String path) {

        List<String[]> res = new ArrayList<>();
        LinkedHashMap<String, Integer> headers = new LinkedHashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String line = reader.readLine();
            int i = 0;
            for (String s : line.split(",")) {
                if (headers.put(s, i++) != null) {
                    System.err.println(s + " Is not unique value into headers");
                    throw new IllegalArgumentException();
                }
            }
            line = reader.readLine();

            for (; line != null; line = reader.readLine()) {
                res.add(line.split(","));
            }

        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return new Struct(res, headers);
    }

    public String getValue(final int i, final int j) {
        return data.get(i)[j];
    }

    public String getValue(final int i, final String header) {
        Integer j = headers.get(header);
        if (j == null) {
            System.err.println(String.format("No header with '%s' name", header));
            throw new NoSuchElementException();
        }
        return getValue(i, j);
    }

    public Set<String> headerList() {
        return headers.keySet();
    }

    public int getSize(){
        return data.size();
    }
}