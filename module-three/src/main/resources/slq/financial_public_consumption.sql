create table consumption
(
    id     serial       not null
        constraint consumption_pkey
            primary key,
    inform varchar(100) not null
        constraint consumption_inform_key
            unique
);

alter table consumption
    owner to postgres;

INSERT INTO public.consumption (id, inform) VALUES (1, 'transport');
INSERT INTO public.consumption (id, inform) VALUES (2, 'food');