package com.alevel.module3.firstmode.entity;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "operations")
public class Operation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Instant date;

    private Integer cost;

    @ManyToOne()
    @JoinColumn(name = "account_id")
    private Account account;

    @ManyToMany(mappedBy = "operations", cascade = CascadeType.ALL)
    private final List<Income> incomes = new ArrayList<>();

    @ManyToMany(mappedBy = "operations", cascade = CascadeType.ALL)
    private final List<Consumption> consumptions = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(int sum) {
        this.cost = sum;
    }

    public List<Income> getIncomes() {
        return incomes;
    }

    public List<Consumption> getConsumptions() {
        return consumptions;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public void addIncome(Income income) {
        income.addOperation(this);
        incomes.add(income);
    }

    public void addConsumption(Consumption consumption) {
        consumption.addOperation(this);
        consumptions.add(consumption);
    }

    @Override
    public String toString() {
        return String.valueOf(cost);
    }
}
