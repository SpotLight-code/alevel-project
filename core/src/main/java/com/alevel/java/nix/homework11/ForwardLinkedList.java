package com.alevel.java.nix.homework11;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.StringJoiner;

public class ForwardLinkedList<T> extends AbstractList<T> implements Iterable<T> {

    private Node<T> head;
    private Node<T> tail;

    private int size = 0;

    @Override
    public boolean add(T t) {

        if (head == null) {
            head = new Node<>(t);
            tail = head;
        }

        tail.next = new Node<>(t);
        tail = tail.next;
        size++;
        return true;
    }

    @Override
    public T set(int index, T element) {
        if (index >= size()) {
            throw new NoSuchElementException(String.format("Index can't be greater than %d", size()));
        }
        Node<T> tempHead = head;

        for (int i = 0; i != index + 1; tempHead = tempHead.next, i++) ;

        T temp = tempHead.data;
        tempHead.data = element;

        return temp;
    }

    @Override
    public void add(int index, T element) {

        if (head == null || index > size) {
            add(element);
        } else {
            Node<T> temp = head, t;
            for (int i = 0; i < index; i++, temp = temp.next) ;
            t = temp.next;
            temp.next = new Node<>(element);
            temp.next.next = t;
            size++;
        }
    }

    @Override
    public T remove(int index) {
        T save;
        Node<T> tempHead = head, prevHead = head;
        if (index > size) {
            throw new IllegalArgumentException("Index can't be > size");
        }
        int i = 0;
        while (i != index + 1) {
            if (tempHead != prevHead) {
                prevHead = prevHead.next;
            }
            tempHead = tempHead.next;
            i++;
        }
        if (tempHead == prevHead) {
            save = tempHead.data;
            head = tempHead.next;
        } else {
            prevHead.next = tempHead.next;
            save = tempHead.data;
        }
        tempHead.data = null;
        size--;
        return save;
    }

    @Override
    public int indexOf(Object o) {
        return super.indexOf(o);
    }

    @Override
    public void clear() {
        super.clear();
    }

    @Override
    public Iterator<T> iterator() {
        return new MyIterator();
    }

    @Override
    public boolean isEmpty() {
        return super.isEmpty();
    }

    @Override
    public boolean remove(Object o) {
        int i = 0;
        for (T t : this) {
            if (o.equals(t)) {
                remove(i);
                return true;
            }
            i++;
        }
        return false;
    }

    @Override
    public T get(int i) {
        int iter = 0;
        T res = null;
        for (T t : this) {
            if (iter++ == i) {
                res = t;
            }
        }
        return res;
    }

    @Override
    public int size() {
        return size;
    }


    private static class Node<T> {

        private T data;
        private Node<T> next;

        public Node(T t) {
            this.data = t;
            next = null;
        }

        public boolean hasNext() {
            return next != null;
        }
    }

    private class MyIterator implements Iterator<T> {
        Node<T> tempHead = head;

        @Override
        public boolean hasNext() {
            return tempHead.hasNext();
        }

        @Override
        public T next() {
            tempHead = tempHead.next;
            return tempHead.data;
        }

    }

    @Override
    public String toString() {
        StringJoiner stringJoiner = new StringJoiner(", ");
        for (T t : this) {
            stringJoiner.add(t.toString());
        }
        return stringJoiner.toString();
    }
}
