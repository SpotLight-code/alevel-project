package com.alevel.java.nix.spring.service;

import com.alevel.java.nix.spring.entity.Task;
import com.alevel.java.nix.spring.entity.newtask.SaveTask;

import java.util.List;
import java.util.Optional;

public interface TaskCRUD {

    Task createNewTask(SaveTask task);

    boolean editTask(int id, SaveTask task);

    Optional<Task> getById(int id);

    Optional<Task> deleteById(int id);

    List<Task> getAllTasks();

    List<Task> getAllUnDoneTasks();

}
