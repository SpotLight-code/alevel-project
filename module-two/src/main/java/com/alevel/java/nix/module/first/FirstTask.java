package com.alevel.java.nix.module.first;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import static java.time.format.DateTimeFormatter.ofPattern;

public class FirstTask {

    private static final DateTimeFormatter[] FORMAT = {ofPattern("dd/MM/yyyy"), ofPattern("yyyy/MM/dd"), ofPattern("MM-dd-yyyy")};

    private static String reformat(String str) throws Exception {

        for (DateTimeFormatter dateTimeFormatter : FORMAT) {
            try {
                return LocalDate.parse(str, dateTimeFormatter).format(ofPattern("yyyyMMdd"));
            } catch (DateTimeParseException ignored) {
            }
        }
        throw new Exception("Can't parse line: " + str);
    }

    public static List<String> reformatDate(List<String> str) {
        return str.stream()
                .collect(
                        ArrayList::new,
                        (List<String> list, String string) -> {
                            try {
                                list.add(reformat(string));
                            } catch (Exception ignored) {
                            }
                        },
                        List::addAll);
    }
}
