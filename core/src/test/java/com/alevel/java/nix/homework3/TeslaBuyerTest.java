package com.alevel.java.nix.homework3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TeslaBuyerTest {

    TeslaBuyer teslaBuyer = new TeslaBuyer();

    @Test
    void setUpValues() {
        assertValues(29.53, 3.32, 3, 4.432, 4.32, 32.53, 1.5, 30.3);
        assertValues(0, 30, 29, 28, 27, 26, 25, 24);
        assertValues(0, 3, 3, 3, 3, 3, 3, 3);
        assertValues(6, 1, 2, 3, 4, 5, 6, 7);
        assertValues(4, 4, 5, 3, 2, 3, 6, 3);
        assertValues(4, 3, 4, 6, 5, 6, 7, 5);
        assertValues(10, 4, 12, 1, 11, 3, 8, 5);
        assertValues(0.22, 2.456, 2.344, 2, 1.54, 1.76);

        assertThrows(Exception.class, () -> teslaBuyer.setUpValues(-1, 0, 1, 2, 4, 5, 6));
    }

    void assertValues(double expected, double... val) {
        assertDoesNotThrow(() -> teslaBuyer.setUpValues(val));
        assertEquals(expected, teslaBuyer.getMaxIncome(), 0.0001);

    }
}