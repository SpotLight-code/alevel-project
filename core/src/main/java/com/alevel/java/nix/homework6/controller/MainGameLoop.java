package com.alevel.java.nix.homework6.controller;

import com.alevel.java.nix.homework6.model.MoveStatus;
import com.alevel.java.nix.homework6.model.TicTacToe;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

public class MainGameLoop{
    private PrintStream out;
    private InputStream in;
    private TicTacToe ticTacToe;

    public MainGameLoop(PrintStream out, InputStream in, TicTacToe ticTacToe) {
        this.out = out;
        this.in = in;
        this.ticTacToe = ticTacToe;
    }

    public void launchGame(){
        MoveStatus moveStatus = new MoveStatus();
        do {
            out.println(ticTacToe);
            out.print(String.format("Move %c: ", ticTacToe.whoNowMove()));
            try {
                Scanner scanner = new Scanner(in);
                int place = scanner.nextInt();
                moveStatus =  ticTacToe.move(place);
            } catch (Exception e) {
                moveStatus.setGameFinished(false);
                out.println(System.lineSeparator() + e.getMessage());
            }
        } while (!moveStatus.isGameFinished());
        out.println(ticTacToe);
        if(moveStatus.isDraw()){
            out.println("The draw was happened!");
        }else{
            out.println(String.format("Winner fond! %c - win", moveStatus.getWinner()));
        }
        ticTacToe.reset();
    }

}
