package com.alevel.module3.firstmode;

import com.alevel.module3.firstmode.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.io.Closeable;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;


public class UserWizard implements Closeable {
    private final static Logger log = LoggerFactory.getLogger(UserWizard.class);

    private final SessionFactory sessionFactory;
    private final Session session;

    private User user;

    private final List<Operation> unconfirmedOperations = new ArrayList<>();

    private final Map<String, Income> allIncomeWays = new HashMap<>();
    private final Map<String, Consumption> allConsumptionWays = new HashMap<>();

    public UserWizard(String userName, String password, String email) {
        try {
            Configuration configuration = new Configuration().configure();
            configuration.setProperty("hibernate.connection.username", userName);
            configuration.setProperty("hibernate.connection.password", password);
            sessionFactory = configuration.buildSessionFactory();
            session = sessionFactory.openSession();
        } catch (Exception e) {
            log.error("Incorrect username or password");
            throw new RuntimeException("Incorrect username or password", e);
        }
        log.info("Connection successful");
        try {
            user = session.bySimpleNaturalId(User.class).load(email);
            Objects.requireNonNull(user);
        } catch (Exception e) {
            log.error("Incorrect email");
            throw new RuntimeException("Incorrect email", e);
        }
        log.info("Authorization successful");
    }


    private  <T> List<T> load(Class<T> tClass) {
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<T> criteria = builder.createQuery(tClass);
        criteria.from(tClass);
        return session.createQuery(criteria).getResultList();
    }


    private void init(Class<?> object) {
        if (object == Income.class) {
            allIncomeWays.putAll(load(Income.class).stream()
                    .collect(Collectors.toMap(Income::getInform, income -> income)));
        } else {
            allConsumptionWays.putAll(load(Consumption.class).stream()
                    .collect(Collectors.toMap(Consumption::getInform, income -> income)));
        }
    }

    public Set<String> getAllIncomeWays() {
        if (allIncomeWays.isEmpty()) {
            init(Income.class);
        }
        return allIncomeWays.keySet();
    }

    public Set<String> getAllConsumptionWays() {
        if (allConsumptionWays.isEmpty()) {
            init(Consumption.class);
        }
        return allConsumptionWays.keySet();
    }


    /**
     * Create new operation and prepare it for loading to database.
     *
     * @param acc  Account which will be affected
     * @param cost Cost of this operation
     * @param info values from IncomeWay or ConsumptionWay, if it is new value, this value will be created
     */

    public void newOperation(int acc, int cost, String... info) {
        if (cost == 0) {
            log.error("Cost can't be 0");
            throw new RuntimeException("Cost can't be 0");
        }
        Objects.checkIndex(acc, user.getAccounts().size());
        Account account = user.getAccounts().get(acc);
        Operation operation = new Operation();
        operation.setAccount(account);
        operation.setDate(Instant.now());
        operation.setCost(cost);
        if (cost > 0) {
            if (allIncomeWays.isEmpty()) {
                init(Income.class);
            }

            for (String s : info) {
                operation.addIncome(allIncomeWays.getOrDefault(s, new Income(s)));
            }
        } else {
            if (allConsumptionWays.isEmpty()) {
                init(Consumption.class);
            }

            for (String s : info) {
                operation.addConsumption(allConsumptionWays.getOrDefault(s, new Consumption(s)));
            }
        }
        unconfirmedOperations.add(operation);
        log.info("Operation saved cost: {}, from balance {}", operation.getCost(), operation.getAccount().getBalance());
    }

    public void cancelOperation(int index) {
        Objects.checkIndex(index, unconfirmedOperations.size());
        var temp = unconfirmedOperations.get(index);
        log.info("Operation canceled cost: {}, from balance {}", temp.getCost(), temp.getAccount().getBalance());
        unconfirmedOperations.remove(index);
    }

    public void confirmOperation(int index) {
        Objects.checkIndex(index, unconfirmedOperations.size());
        Operation operation = unconfirmedOperations.get(index);
        int cost = operation.getCost();

        if (cost < 0 && operation.getAccount().getBalance() < cost * (-1)) {
            log.error("No enough money");
            throw new RuntimeException("No enough money");
        }
        operation.getAccount().setBalance(operation.getAccount().getBalance() + cost);
        operation.getAccount().addOperation(operation);
        unconfirmedOperations.remove(index);
        log.info("Operation confirmed id: {}, cost: {}", operation.getId(), operation.getCost());
        log.info("Operation ready to save to database");
        try {
            save();
        } catch (Exception e) {
            operation.getAccount().setBalance(operation.getAccount().getBalance() - cost);
            unconfirmedOperations.add(operation);
            log.error("all changes canceled");
            throw new RuntimeException(e);
        }
    }

    public void save() {
        try {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
            log.info("Operation saved!");
        } catch (Exception e) {
            session.getTransaction().rollback();
            log.error("Failed updating database! Your operation hasn't been saved!");
            throw new RuntimeException(e);
        }
    }

    public void reload() {
        String email = user.getEmail();
        User backup = user;
        log.info("Loading data...");

        try {
            user = session.bySimpleNaturalId(User.class).load(email);
            Objects.requireNonNull(user);
            log.info("Updating successful!");
            allConsumptionWays.clear();
            allIncomeWays.clear();
        } catch (Exception e) {
            log.error("Failed updating info! Nothing changed!", e);
            user = backup;
        }
    }

    public List<Account> getAccounts() {
        return user.getAccounts();
    }

    @Override
    public void close() {
        sessionFactory.close();
        session.close();
    }

    public List<Operation> getUnconfirmedOperations() {
        return unconfirmedOperations;
    }
}
