package com.alevel.java.nix.hippodrome;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.stream.Collectors;

public class Race {

    private final Horse[] horses;

    private final CountDownLatch latch;

    private List<Horse> sortedFinishedList;

    public Race(int countHorses) {
        latch = new CountDownLatch(countHorses);
        CyclicBarrier cyclicBarrier = new CyclicBarrier(countHorses);
        horses = new Horse[countHorses];
        for (int i = 0; i < countHorses; i++) {
            horses[i] = new Horse(i + 1, cyclicBarrier, latch);
        }
    }

    public int startRiding(int probableWinHorse) {
        Objects.checkIndex(probableWinHorse - 1, horses.length);

        for (Horse hors : horses) {
            new Thread(hors).start();
        }

        try {
            latch.await();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        sortedFinishedList =  Arrays.stream(horses)
                .filter(Horse::isNotDisqualified)
                .sorted()
                .collect(Collectors.toList());

        int horsePos = sortedFinishedList.indexOf(horses[probableWinHorse - 1]) + 1;

        if(horsePos == 0){
            throw new RuntimeException("Horse Disqualified");
        }
        return horsePos;
    }

    public List<Horse> getSortedFinishedList() {
        return sortedFinishedList;
    }
}
