package com.alevel.java.nix.spring.database;

import com.alevel.java.nix.spring.entity.Task;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Integer> {
    List<Task> getTasksByDoneFalse();
}
