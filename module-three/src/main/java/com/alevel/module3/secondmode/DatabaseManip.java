package com.alevel.module3.secondmode;

import com.alevel.module3.secondmode.model.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.sql.*;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DatabaseManip implements Closeable {
    private final Connection connection;
    private final static Logger log = LoggerFactory.getLogger(DatabaseManip.class);
    private final String email;
    private final Instant before;
    private final Instant finish;

    private final List<Result> results = new ArrayList<>();

    public DatabaseManip(String userName, String password, String email, Instant before, Instant finish) {

        if (finish.isBefore(before)) {
            log.error("Invalid data parameters!");
            throw new RuntimeException("Invalid data parameters!");
        }
        try {
            connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/financial", userName, password);
            log.info("Authorization successful!");
        } catch (SQLException e) {
            log.error("Failed while authorisation, invalid login or password");
            throw new RuntimeException("Failed while authorisation, invalid login or password", e);
        }
        this.email = email;
        this.before = before;
        this.finish = finish;

        generateResultList();
    }

    private void generateResultList() {

        String sql = "select cost, date from operations " +
                "join accounts on account_id = accounts.id " +
                "join users on user_id = users.id where email = ? ";

        String dateSql = "and date between ? and ?";

        boolean isAllTimeNeed = finish.equals(Instant.MAX);

        try (PreparedStatement preparedStatement = connection.prepareStatement(isAllTimeNeed ? sql : sql + dateSql)) {
            preparedStatement.setString(1, email);
            if (!isAllTimeNeed) {
                preparedStatement.setTimestamp(2, Timestamp.from(before));
                preparedStatement.setTimestamp(3, Timestamp.from(finish));
            }
            ResultSet resultSet = preparedStatement.executeQuery();

            resultSet.next();

            int cost = resultSet.getInt("cost");
            Result prevRes = new Result(resultSet.getTimestamp("date").toLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
                    cost, 0, 0);
            results.add(prevRes);

            while (resultSet.next()) {
                cost = resultSet.getInt("cost");

                Result current = new Result(resultSet.getTimestamp("date").toLocalDateTime().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME),
                        cost, prevRes.getSum(), prevRes.getBalance());
                results.add(current);
                prevRes = current;

            }
            log.info("Loaded {} operations", results.size());
            resultSet.close();
        } catch (SQLException e) {
            log.error("Error from database", e);
        }
    }

    public List<Result> getResults() {
        return results;
    }


    @Override
    public void close() {
        try {
            connection.close();
            log.info("Connection closed");
        } catch (SQLException e) {
            log.error("Error happened while closing connection", e);
        }
    }
}
