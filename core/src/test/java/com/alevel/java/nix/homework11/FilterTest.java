package com.alevel.java.nix.homework11;

import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class FilterTest {

    @Test
    void getNumberFromStrings() {
        assertDoesNotThrow(() -> assertEquals(2345, Filter.getNumberFromStrings(" jig i2jkh3oihoi4", "fsdfds", "", "5").getAsLong()));
        assertDoesNotThrow(() -> assertEquals(0, Filter.getNumberFromStrings(" jig i0jkhoihoi", "fsdfds", "", "rwe").getAsLong()));

        assertDoesNotThrow(() -> assertTrue(Filter.getNumberFromStrings(" jig ijkhoihoi", "fsdfds", "", "").isEmpty()));
        assertDoesNotThrow(() -> assertTrue(Filter.getNumberFromStrings().isEmpty()));

        assertThrows(NoSuchElementException.class, () -> assertEquals(0, Filter.getNumberFromStrings().getAsLong()));
        assertDoesNotThrow(() -> assertFalse(Filter.getNumberFromStrings(" ji2g ijkhoihoi", "fsdfds", "", "").isEmpty()));

    }
}