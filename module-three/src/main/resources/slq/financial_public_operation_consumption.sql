create table operation_consumption
(
    operation_id   integer
        constraint account_consumption_operation_id_fkey
            references operations,
    consumption_id integer
        constraint account_consumption_consumption_id_fkey
            references consumption
);

alter table operation_consumption
    owner to postgres;

