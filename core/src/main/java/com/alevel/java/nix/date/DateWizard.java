package com.alevel.java.nix.date;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static java.util.stream.Collectors.*;

public class DateWizard {

    public static SortedMap<LocalDate, SortedSet<LocalTime>> groupUp(Collection<LocalDateTime> list){
        return list.stream()
                .collect(groupingBy(
                                LocalDateTime::toLocalDate,
                                TreeMap::new,
                                mapping(LocalDateTime::toLocalTime, toCollection(TreeSet::new))));

    }


    public static Duration getMaxDurationStream(Collection<LocalDateTime> list){
        return list.stream()
                .map(MinAndMax::init)
                .reduce(MinAndMax::setUpValues)
                .map((a) -> Duration.between(a.getMin(), a.getMax()))
                .orElse(Duration.ZERO);
    }

    public static Duration getMaxDuration(Collection<LocalDateTime> list){
        if(list.isEmpty()){
            return Duration.ZERO;
        }
        var iter = list.iterator();

        LocalDateTime min;
        LocalDateTime max = min = iter.next();

        while(iter.hasNext()){
            LocalDateTime temp = iter.next();
            if(min.compareTo(temp) > 0){
                min = temp;
            }else if(max.compareTo(temp) < 0){
                max = temp;
            }
        }
        return Duration.between(min,max);
    }

}
