package com.alevel.java.nix.homework9;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RetryTest {
    static int iteration;
    @Test
    void successfulRetry() {
        iteration = 0;
        assertDoesNotThrow(() -> {

            Retry retry = new Retry(10, 100);
            retry.launch(() -> {
                if (iteration++ < 8) {
                    throw new Exception("Exception");
                }
            });

        });
    }

    @Test
    void unsuccessfulRetry() {
        iteration = 0;
        assertEquals("Exception", assertThrows(Exception.class, () -> {
            Retry retry = new Retry(1, 100);
            retry.launch(() -> {
                if (iteration++ < 8) {
                    throw new Exception("Exception");
                }
            });
        }).getMessage());
    }

    @Test
    void badInit() {
        assertThrows(RuntimeException.class, () -> new Retry(0, 0));
        assertThrows(RuntimeException.class, () -> new Retry(0, 1));
        assertThrows(RuntimeException.class, () -> new Retry(1, 0));
    }

}