package com.alevel.java.nix.practice1.gallows;

public interface Gallows {

    Status makeMove(char symbol);

    String showWord();

    int getChancesCount();

    boolean isGameFinished();
}
