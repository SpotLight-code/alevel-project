package com.alevel.java.nix.practice1.gallows.launcher;

import com.alevel.java.nix.practice1.gallows.Gallows;
import com.alevel.java.nix.practice1.gallows.GallowsGame;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Launcher {
    private List<String> allWord;
    private static Random random = new Random();
    private PrintStream printStream = System.out;

    public Launcher(List<String> words) {
        allWord = words;
    }

    public Gallows getGame() {
        return new GallowsGame(allWord.get(random.nextInt(allWord.size())));
    }

    public Gallows getGame(int chances) {
        return new GallowsGame(allWord.get(random.nextInt(allWord.size())), chances);
    }

    public void setPrintStream(PrintStream printStream) {
        this.printStream = printStream;
    }

    public static Launcher launchFromRecourse(String name) {
        InputStream is = Launcher.class.getResourceAsStream(name);
        InputStreamReader inputStreamReader = new InputStreamReader(is);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        List<String> list = new ArrayList<>();
        try {
            for (String i = bufferedReader.readLine(); i != null; i = bufferedReader.readLine()) {
                list.add(i);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        return new Launcher(list);
    }

    public void print(String msg) {
        printStream.println(msg);
    }

    public static void singleLoop(Launcher launcher, InputStream inputStream){
        Scanner scanner = new Scanner(inputStream);
        var game = launcher.getGame();
        while (!game.isGameFinished()) {
            launcher.print(game.showWord());
            launcher.print(String.format("You have %d chances, enter character: ", game.getChancesCount()));
            game.makeMove(scanner.nextLine().charAt(0));
        }
        if (game.getChancesCount() == 0) {
            launcher.print("You lose correct word: " + game.showWord());
        } else {
            launcher.print("You win! Word: " + game.showWord());
        }

    }

    public static void main(String[] args) {
        singleLoop(launchFromRecourse("/forTest.txt"), System.in);
    }

}