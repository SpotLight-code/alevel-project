package com.alevel.java.nix.practice1.gallows;

import java.util.Arrays;

public class GallowsGame implements Gallows {
    private char[] currentWord;
    private final String word;
    private Status status;

    public GallowsGame(String word, int chances) {
        if (chances < 0) {
            throw new IllegalArgumentException("Value mus't be less than 0");
        }
        this.word = word;
        currentWord = new char[word.length()];
        Arrays.fill(currentWord, '*');

        status = new Status(String.valueOf(currentWord), chances, word);
    }

    public GallowsGame(String word) {
        this.word = word;
        currentWord = new char[word.length()];
        Arrays.fill(currentWord, '*');
        status = new Status(String.valueOf(currentWord), 5, word);
    }

    @Override
    public Status makeMove(char symbol) {

        if (isGameFinished()) {
            throw new RuntimeException("Game Finished");
        }
        boolean isFond = false;

        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == symbol && currentWord[i] == '*') {
                isFond = true;
                currentWord[i] = symbol;
            }
        }
        if (!isFond) {
            status.decrementChances();
            return status;
        }

        status.setCurrentWord(String.valueOf(currentWord));

        return status;
    }

    @Override
    public String showWord() {
        return status.isGameFinished() ? status.getCorrectWord() : status.getCurrentWord();
    }

    @Override
    public int getChancesCount() {
        return status.getChances();
    }

    @Override
    public boolean isGameFinished(){
        return status.isGameFinished();
    }
}
