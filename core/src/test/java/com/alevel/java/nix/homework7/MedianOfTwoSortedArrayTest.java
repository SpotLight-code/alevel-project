package com.alevel.java.nix.homework7;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MedianOfTwoSortedArrayTest {
    MedianOfTwoSortedArray medianOfTwoSortedArray = new MedianOfTwoSortedArray();

    @Test
    void findMedianSortedArrays() {
        int[][] arr1 = {
                {1,2,3,4},
                {},
                {2,3,9},
                {3,4,5},
                {1,2,3}
        };
        int[][] arr2 = {
                {5,6},
                {1,2,3},
                {2,3,9},
                {},
                {4,5}
        };

        double[] res = {3.5,2.0,3,4,3};
        for(int i = 0; i < res.length; i++){
            assertMedian(res[i], arr1[i], arr2[i]);
        }
    }

    void assertMedian(double exp, int[] arr1, int[] arr2){
        assertEquals(exp,medianOfTwoSortedArray.findMedianSortedArrays(arr1,arr2),0.0000001);
    }
}