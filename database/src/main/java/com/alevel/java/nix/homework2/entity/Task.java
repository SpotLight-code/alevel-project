package com.alevel.java.nix.homework2.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "problems")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "from_id")
    private City from;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "to_id")
    private City to;

    @OneToOne(mappedBy = "task")
    private FoundRoute foundRoute;

    @OneToOne(mappedBy = "task")
    private ImpossibleRoute impossibleRoute;

    public Task(){}

    public Task(City from, City to) {
        this.from = from;
        this.to = to;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public City getFrom() {
        return from;
    }

    public void setFrom(City from) {
        this.from = from;
    }

    public City getTo() {
        return to;
    }

    public void setTo(City to) {
        this.to = to;
    }

    public FoundRoute getFoundRoute() {
        return foundRoute;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return from.equals(task.from) && to.equals(task.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, from);
    }

    @Override
    public String toString() {
        return "Task{" +
                "from=" + from.getName() +
                ", to=" + to.getName() +
                '}';
    }
}
