package com.alevel.java.nix.module.third;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ThirdTaskTest {

    @Test
    void nextRound() {
        boolean[][] start = {
                {true, true, true},
                {true, true, true},
                {true, true, true}
        };
        ThirdTask thirdTask = new ThirdTask(start);


        thirdTask.nextRound();
        boolean[][] end = {
                {true, false, true},
                {false, false, false},
                {true, false, true}
        };
        assertArr(end, thirdTask.getDesk());


        thirdTask.nextRound();
        boolean[][] end1 = {
                {false, false, false},
                {false, false, false},
                {false, false, false}
        };
        assertArr(end1, thirdTask.getDesk());

        boolean[][] newStart = {
                {true, false, true},
                {false, true, false},
                {false, false, false}
        };
        boolean[][] newEnd = {
                {false, true, false},
                {false, true, false},
                {false, false, false}
        };
        thirdTask.setDesk(newStart);
        thirdTask.nextRound();
        assertArr(newEnd, thirdTask.getDesk());
        thirdTask.nextRound();
        assertArr(end1, thirdTask.getDesk());
    }

    void assertArr(boolean[][] exp, boolean[][] actual) {
        for (int i = 0; i < exp.length; i++) {
            assertArrayEquals(exp[i], actual[i]);
        }
    }
}