package com.alevel.java.nix.homework6.model;

public class MoveStatus {
    private char winner;
    private boolean isDraw;
    private boolean isGameFinished;

    public void setWinner(char winner) {
        this.winner = winner;
    }

    public void setDraw(boolean draw) {
        isDraw = draw;
    }

    public boolean isGameFinished() {
        return isGameFinished;
    }

    public void setGameFinished(boolean gameFinished) {
        isGameFinished = gameFinished;
    }

    public char getWinner() {
        return winner;
    }

    public boolean isDraw() {
        return isDraw;
    }
}
