package com.alevel.module3.firstmode;

import java.util.Scanner;
import java.util.StringJoiner;

public class UiLoop {
    public void begin(String userName, String password, String email) {
        try (UserWizard userWizard = new UserWizard(userName, password, email)) {
            Scanner scanner = new Scanner(System.in);
            StringBuilder menu = new StringBuilder()
                    .append("1 - Create new operation").append(System.lineSeparator())
                    .append("2 - Confirm operation").append(System.lineSeparator())
                    .append("3 - Cancel operation").append(System.lineSeparator())
                    .append("0 - exit");


            while (true) {
                try {
                    System.out.println(menu.toString());
                    int value = scanner.nextInt();
                    switch (value) {
                        case 0:
                            return;
                        case 1:
                            createNewOperation(userWizard, scanner);
                            break;
                        case 2:
                            confirmOperation(userWizard, scanner);
                            break;
                        case 3:
                            cancelOperation(userWizard, scanner);
                            break;
                    }
                } catch (Exception ignored) {
                    scanner.nextLine();
                }
            }

        }
    }

    private String loadUnconfined(UserWizard userWizard) {
        StringJoiner stringJoiner = new StringJoiner(", ");
        var list = userWizard.getUnconfirmedOperations();

        for (int i = 0; i < list.size(); i++) {
            stringJoiner.add(String.format("[%d -> %d from balance %s]",
                    i, list.get(i).getCost(), list.get(i).getAccount().getBalance()));
        }
        return stringJoiner.length() == 0 ? "No one operation" : stringJoiner.toString();
    }

    private void cancelOperation(UserWizard userWizard, Scanner scanner) {
        while (true) {
            try {
                System.out.println("Chose operation or -1 for return: " + loadUnconfined(userWizard));
                int id = scanner.nextInt();
                if (id == -1) {
                    return;
                }
                userWizard.cancelOperation(id);
                break;
            } catch (Exception e) {
                System.out.println("Error happened " + e.getMessage());
                scanner.nextLine();
            }
        }
    }

    private void confirmOperation(UserWizard userWizard, Scanner scanner) {
        while (true) {
            try {
                System.out.println("Chose operation or -1 for return: " + loadUnconfined(userWizard));
                int id = scanner.nextInt();
                if (id == -1) {
                    return;
                }
                userWizard.confirmOperation(id);
                break;
            } catch (Exception e) {
                System.out.println("Error happened " + e.getMessage());
                scanner.nextLine();
            }
        }
    }

    private void createNewOperation(UserWizard userWizard, Scanner scanner) {

        StringJoiner stringJoiner = new StringJoiner(", ");
        var list = userWizard.getAccounts();
        for (int i = 0, size = list.size(); i < size; i++) {
            stringJoiner.add("id: " + i + " -> balance = " + list.get(i).getBalance());
        }

        try {
            System.out.println(String.format("Select account: %s", stringJoiner.toString()));
            int accId = scanner.nextInt();
            System.out.print("Enter cost: ");
            int cost = scanner.nextInt();
            System.out.println("Show possible operation types? y/n");
            char res = scanner.next().charAt(0);
            if (res == 'y') {
                System.out.println(cost > 0 ? userWizard.getAllIncomeWays() : userWizard.getAllConsumptionWays());
            }
            System.out.println("Enter all types of this operation, use `,[space]` like delimiter");

            String info = scanner.next();
            info += scanner.nextLine();

            userWizard.newOperation(accId, cost, info.split(", "));

        } catch (Exception e) {
            System.out.println("Error happened " + e.getMessage());
        }
    }
}




