package com.alevel.java.nix.module.second;

import java.util.ArrayDeque;

public class SecondTask {


    public boolean isStringValid(String string) {
        String open = "({[";
        String close = ")}]";

        ArrayDeque<Character> openScopes = new ArrayDeque<>();

        for (int i = 0, len = string.length(); i < len; i++) {
            char temp = string.charAt(i);

            if (open.indexOf(temp) != -1) {
                openScopes.addLast(temp);
                continue;
            }

            if (openScopes.isEmpty() || openScopes.removeLast() != open.charAt(close.indexOf(temp))) {
                return false;
            }

        }

        return openScopes.isEmpty();
    }


    public int getMaxCountOfBinaryTree(TreeNode treeNode) {
        if (treeNode == null) {
            return 0;
        }
        int maxValue = 0;

        return getNodes(treeNode, maxValue);
    }

    private int getNodes(TreeNode treeNode, int maxValue) {
        maxValue++;

        int left = maxValue;
        int right = maxValue;

        if (treeNode.left != null) {
            left = getNodes(treeNode.left, maxValue);
        }
        if (treeNode.right != null) {
            right = getNodes(treeNode.right, maxValue);
        }

        return Math.max(left, right);
    }

}
