package com.alevel.java.nix.hippodrome;

public class Constants {
    public final static int MIN_SLEEP = 400;
    public final static int MAX_SLEEP = 500;

    public final static int DISTANCE = 1000;

    public final static int MIN_DISTANCE = 100;
    public final static int MAX_DISTANCE = 200;
}
