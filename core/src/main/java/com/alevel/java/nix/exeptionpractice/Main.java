package com.alevel.java.nix.exeptionpractice;

public class Main {
    public static void main(String[] args) {

        try {
            ArithmeticProgression arithmeticProgression = new ArithmeticProgression(0, 2);
            System.out.println(arithmeticProgression.calculate(100));
        } catch (ProgressionConfigurationException e) {
            e.printStackTrace();
        }

        try {
            ArithmeticProgression arithmeticProgression = new ArithmeticProgression(0, 0);
            System.out.println(arithmeticProgression.calculate(100));
        } catch (ProgressionConfigurationException e) {
            e.printStackTrace();
        }

        try {
            ArithmeticProgression arithmeticProgression = new ArithmeticProgression(0, 2);
            System.out.println(arithmeticProgression.calculate(-100));
        } catch (ProgressionConfigurationException e) {
            e.printStackTrace();
        }

    }
}
