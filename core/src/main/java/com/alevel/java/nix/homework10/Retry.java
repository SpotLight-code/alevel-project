package com.alevel.java.nix.homework10;

import ch.qos.logback.classic.Logger;

import org.slf4j.LoggerFactory;

public class Retry {
    final private int countOfTries;
    final private int firstTimeSleep;
    final static Logger log = (Logger) LoggerFactory.getLogger(Retry.class);

    public Retry(int countOfTries, int firstTimeSleep) {

        if (countOfTries <= 0) {
            throw new IllegalArgumentException("Count of tries mustn't be less than 0");
        }
        if (firstTimeSleep <= 0) {
            throw new IllegalArgumentException("First time of sleep can't be less than 0");
        }

        this.countOfTries = countOfTries;
        this.firstTimeSleep = firstTimeSleep;
    }

    public <T> T launch(Block<T> block) throws Exception {
        Exception exception;
        T t;
        if (countOfTries == 1) {
            try {
                t = block.run();
                log.info("Single try was successful");
            } catch (Exception e) {
                log.warn("Single try was unsuccessful");
                throw e;
            }
            return t;
        }

        try {
            t = block.run();
            log.info("Success on first try!");
            return t;
        } catch (Exception e) {
            exception = e;
            log.warn("First try was unsuccessful");
        }

        for (int i = 1; i < countOfTries; i++) {
            try {
                log.info("Waiting for {} ms", firstTimeSleep * i);
                Thread.sleep(firstTimeSleep * i);
                t = block.run();
                log.info(String.format("Success on %d try", i + 1));
                return t;
            } catch (Exception e) {
                exception = e;
                log.warn("{} try was unsuccessful", i + 1);
            }
        }
        log.error("All tries failed!", exception);
        throw exception;
    }
}