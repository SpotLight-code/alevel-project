package com.alevel.java.nix.homework10;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RetryTest {


     static class test implements Block<String>{
         int a;

         test(int a){
             this.a = a;
         }
        @Override
        public String run() throws Exception {
            if(a-- > 0){
                throw new Exception("");
            }else {
                return "hello";
            }
        }
    }

    @Test
    void launchTest() {
        Retry retry = new Retry(6, 100);

        assertDoesNotThrow(() -> assertEquals("hello", retry.launch(() -> "hello")));
        assertDoesNotThrow(() -> assertEquals("hello", retry.launch(new test(5))));
        assertThrows(Exception.class, () -> retry.launch(new test(10)));
    }

    @Test
    void badInit() {
        assertThrows(RuntimeException.class, () -> new com.alevel.java.nix.homework9.Retry(0, 0));
        assertThrows(RuntimeException.class, () -> new com.alevel.java.nix.homework9.Retry(0, 1));
        assertThrows(RuntimeException.class, () -> new com.alevel.java.nix.homework9.Retry(1, 0));
    }
}