package com.alevel.java.nix.homework8.aggregation;

import java.util.Scanner;

public class SubstanceConverter {

    public static void launch(final BaseSubstance... baseSubstance) {

        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append(("Enter what you want to heat up:")).append(System.lineSeparator());
        for (int i = 0; i < baseSubstance.length; i++) {
            stringBuilder.append(String.format("%d - %s", i + 1, baseSubstance[i].getName()));
            stringBuilder.append(System.lineSeparator());
        }
        stringBuilder.append(("0 - exit from program")).append(System.lineSeparator());

        Scanner scanner = new Scanner(System.in);
        int index = 0;

        do {
            System.out.println(stringBuilder);

            try {
                index = scanner.nextInt();
                if (index < 0 || index > 3) {
                    throw new Exception();
                }

                System.out.println("Enter new temperature for " + baseSubstance[index - 1].getName());
                baseSubstance[index - 1].heatUp(scanner.nextDouble());
                System.out.println(baseSubstance[index - 1]);

            } catch (Exception ignored) {
                System.out.println("Enter valid argument!");
            }

        } while (index != 0);

    }


    public static void main(final String[] args) {
        BaseSubstance[] baseSubstance = {new Oxygen(), new Iron(), new Water()};
        launch(baseSubstance);
    }

}
