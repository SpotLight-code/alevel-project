package com.alevel.java.nix.homework1;

import java.io.Closeable;
import java.util.List;

public class CityWizard implements Closeable {
    final private List<City> cities;
    final private List<Task> task;
    final private JDBCRunner database;

    private CityWizard(List<City> cities, List<Task> task, JDBCRunner database) {
        this.cities = cities;
        this.task = task;
        this.database = database;
    }

    public static CityWizard initFromDatabase(JDBCRunner database) {
        List<City> cities = database.getCities();

        for (City c : cities) {
            c.addAllNeighbor(database.getAllNeighborsByCityId(c.getId()));
        }
        return new CityWizard(cities, database.getTasks(cities), database);
    }

    public int[] findAllTasks() {

        int[] res = new int[task.size()];
        for (int i = 0; i < res.length; i++) {
            res[i] = task.get(i).resolveProblem(cities);

            if (res[i] != Integer.MAX_VALUE) {
                database.resolveTask(task.get(i), res[i]);
            } else {
                database.unreachableTask(task.get(i));
            }
        }
        return res;
    }

    @Override
    public void close() {
        database.close();
    }
}
