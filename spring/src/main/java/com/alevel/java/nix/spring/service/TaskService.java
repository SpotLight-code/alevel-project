package com.alevel.java.nix.spring.service;

import com.alevel.java.nix.spring.database.TaskRepository;
import com.alevel.java.nix.spring.entity.Task;
import com.alevel.java.nix.spring.entity.newtask.SaveTask;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskService implements TaskCRUD{
    private final TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task createNewTask(SaveTask task) {
        Task t = new Task();
        t.setDone(task.isDone());
        t.setDescription(task.getDescription());

        taskRepository.save(t);
        return  t;
    }

    @Override
    public boolean editTask(int id, SaveTask task) {
        Optional<Task> taskForEdit = taskRepository.findById(id);
        if (taskForEdit.isEmpty()) {
            return false;
        }
        Task t = taskForEdit.get();
        t.setDescription(task.getDescription());
        t.setDone(task.isDone());
        taskRepository.save(t);
        return true;
    }

    @Override
    public Optional<Task> getById(int id) {
        return taskRepository.findById(id);
    }

    @Override
    public Optional<Task> deleteById(int id) {
        var task = getById(id);
        task.ifPresent(taskRepository::delete);
        return task;
    }

    @Override
    public List<Task> getAllTasks() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> getAllUnDoneTasks() {
        return taskRepository.getTasksByDoneFalse();
    }

}
