package com.alevel.java.nix.homework2;

import com.alevel.java.nix.homework2.entity.City;
import com.alevel.java.nix.homework2.entity.Connection;
import com.alevel.java.nix.homework2.entity.Task;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LoaderTest {

    private final Loader loader = new Loader("input.txt");;

    private static final List<City> cities = new ArrayList<>(3);
    private static final List<Connection> connections = new ArrayList<>(4);
    private static final List<Task>  tasks = new ArrayList<>();

    @BeforeAll
    static void init(){
        cities.addAll(Arrays.asList(
                new City("Kiev"),
                new City("Poltava"),
                new City("Odessa")
        ));

        connections.addAll(Arrays.asList(
                new Connection(3, cities.get(0), cities.get(1)),
                new Connection(5, cities.get(0), cities.get(2)),
                new Connection(3, cities.get(1), cities.get(0)),
                new Connection(5, cities.get(2), cities.get(0))
        ));

        cities.get(0).addConnection(connections.get(0));
        cities.get(0).addConnection(connections.get(1));
        cities.get(1).addConnection(connections.get(2));
        cities.get(2).addConnection(connections.get(3));

        tasks.addAll(Arrays.asList(
                new Task(cities.get(0),cities.get(1)),
                new Task(cities.get(0),cities.get(2)),
                new Task(cities.get(1),cities.get(2))
        ));
    }

    @Test
    void getCities() {
        assertEquals(cities, loader.getCities());
    }

    @Test
    void getConnections() {
        assertEquals(connections, loader.getConnections());
    }

    @Test
    void getTasks() {
        assertEquals(tasks, loader.getTasks());
    }
}