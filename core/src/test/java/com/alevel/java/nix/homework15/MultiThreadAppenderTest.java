package com.alevel.java.nix.homework15;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class MultiThreadAppenderTest {

    @Test
    void append() throws InterruptedException {
        MultiThreadAppender appender = new MultiThreadAppender();
        StringBuilder stringBuilder = new StringBuilder();

        stringBuilder.append("hello");
        appender.append("hello");
        assertEquals(stringBuilder.toString(), appender.getInput());


        stringBuilder.append("hello124");
        appender.append("hello124");
        assertEquals(stringBuilder.toString(), appender.getInput());

        stringBuilder.append("hlo");
        appender.append("hlo");
        assertEquals(stringBuilder.toString(), appender.getInput());

        stringBuilder.append("ho");
        appender.append("ho");
        assertEquals(stringBuilder.toString(), appender.getInput());

        appender.append("quit");

        Thread.sleep(1000);
        File file = new File("output.txt");
        assertTrue(file.isFile());
        assertTrue(file.canRead());
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            reader.readLine().equals(stringBuilder.toString());
            assertNull(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}