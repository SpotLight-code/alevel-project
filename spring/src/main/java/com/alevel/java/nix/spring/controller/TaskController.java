package com.alevel.java.nix.spring.controller;

import com.alevel.java.nix.spring.entity.Task;
import com.alevel.java.nix.spring.entity.newtask.SaveTask;
import com.alevel.java.nix.spring.service.TaskCRUD;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tasks")
public class TaskController {
    private final TaskCRUD tasks;

    public TaskController(TaskCRUD tasks) {
        this.tasks = tasks;
    }

    @GetMapping("/{id}")
    public Task getById(@PathVariable int id){
        return tasks.getById(id).orElseThrow();
    }
    @GetMapping("/all")
    public List<Task> getAll(){
        return tasks.getAllTasks();
    }



    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public Task create(@RequestBody SaveTask task){
       return tasks.createNewTask(task);
    }

    @DeleteMapping("/{id}")
    public Task delete(@PathVariable int id){
        return tasks.deleteById(id).orElseThrow();
    }


    @PutMapping("/{id}")
    public void update(@PathVariable int id, @RequestBody SaveTask task){
        if (!tasks.editTask(id, task)) {
            throw new RuntimeException("Not found");
        }
    }
    @PatchMapping("/{id}")
    public void setDone(@PathVariable int id){
        var task = getById(id);
        tasks.editTask(id, new SaveTask(task.getDescription(), true));

    }

    @GetMapping("/notdone")
    public List<Task> getAllNotDone(){
        return tasks.getAllUnDoneTasks();
    }

}
