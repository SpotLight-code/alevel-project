package com.alevel.java.nix.filestream;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;


import static java.nio.file.Paths.get;

public class FileFilter {
    public static void printLinesWhichContainSubStr(String path, String subString){
        try {
            Files.lines(get(path))
                    .filter(s -> s.contains(subString))
                    .forEach(System.out::println);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}
