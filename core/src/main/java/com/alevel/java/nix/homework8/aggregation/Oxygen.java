package com.alevel.java.nix.homework8.aggregation;

public class Oxygen extends BaseSubstance {
    private final static double toVigor = -218.3;
    private final static double toGas = -182.9;
    private final static String name = "Oxygen";

    @Override
    double getVigorTemperature() {
        return toVigor;
    }

    @Override
    double getGasTemperature() {
        return toGas;
    }

    @Override
    public String getName() {
        return name;
    }
}
