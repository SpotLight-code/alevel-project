package com.alevel.java.nix.homework2;

import com.alevel.java.nix.homework2.entity.Task;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static com.alevel.java.nix.homework2.HibernateLoader.loadUnresolvedTasks;
import static org.junit.jupiter.api.Assertions.assertEquals;

class MainTest {

    private static final Map<Integer, List<Integer>> testRes = Map.ofEntries(
            Map.entry(0, List.of(1, 4)),
            Map.entry(1, List.of(2, 4)),
            Map.entry(2, List.of(2, 3)),
            Map.entry(4, List.of(2, 5)),
            Map.entry(5, List.of(5, 2))
    );

    private static final Map<Integer, Integer> resultTask = Map.ofEntries(
            Map.entry(0, 3),
            Map.entry(1, 2),
            Map.entry(5, 4),
            Map.entry(2, 1),
            Map.entry(4, Integer.MAX_VALUE)
    );


    @Test
    void loadData() {

        Configuration cfg = new Configuration().configure();

        try (Session session = cfg.buildSessionFactory().openSession()) {
            List<Task> taskList = loadUnresolvedTasks(session);

            assertEquals(testRes.size(), taskList.size());
            for (Task task : taskList) {
                List<Integer> list = testRes.get(task.getId());
                assertEquals(list.get(0), task.getFrom().getId());
                assertEquals(list.get(1), task.getTo().getId());

                int res = new ProblemFinder(task).findMinPath();

                int cost = resultTask.get(task.getId());
                assertEquals(cost, res);
            }
        }
    }
}