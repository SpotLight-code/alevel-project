package com.alevel.java.nix.homework11;

import java.util.Arrays;
import java.util.OptionalLong;


public class Filter {

    public static OptionalLong getNumberFromStrings(String... str) {
        return Arrays.stream(str)
                .flatMapToInt(String::codePoints)
                .filter(Character::isDigit)
                .asLongStream()
                .map(l -> Character.digit((int) l, 10))
                .reduce((l, l1) -> l * 10 + l1);
    }
}
