package com.alevel.java.nix.homework2.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "foundroutes")
public class FoundRoute {
    @Id
    private Integer id;

    @OneToOne
    @MapsId
    @JoinColumn(name = "id")
    private Task task;

    private Integer cost;

    public FoundRoute(Task task, Integer cost) {
        this.task = task;
        this.cost = cost;
    }

    public FoundRoute() {
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FoundRoute that = (FoundRoute) o;
        return task.equals(that.task);
    }

    @Override
    public int hashCode() {
        return Objects.hash(task);
    }
}
