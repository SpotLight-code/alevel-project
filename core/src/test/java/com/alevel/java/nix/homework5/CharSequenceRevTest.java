package com.alevel.java.nix.homework5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharSequenceRevTest {

    @Test
    void reverse() {
        assertStrings("0987654321", "1234567890");
        assertStrings("123", "321");
        assertStrings("12345".repeat(100), "54321".repeat(100));
        assertStrings("", "");
        assertStrings("a", "a");

        assertThrows(IllegalArgumentException.class, () -> CharSequenceRev.reverse("res").subSequence(-1, 1));
        assertEquals("tup", CharSequenceRev.reverse("input").subSequence(0, 3).toString());
    }

    void assertStrings(String exp, String test) {
        assertEquals(exp, CharSequenceRev.reverse(test).toString());
    }

}