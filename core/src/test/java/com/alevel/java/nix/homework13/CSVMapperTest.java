package com.alevel.java.nix.homework13;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class CSVMapperTest {

    @Test
    void loadList() {
        List<Example> actual = new CSVMapper<>(Struct.load("example.csv"), Example.class).getList();

        List<Example> expected = new ArrayList<>();

        expected.add(new Example("Tom", 13));
        expected.add(new Example("Peter", 19));
        expected.add(new Example("Jack", 30));

        assertEquals(expected, actual);

    }
}