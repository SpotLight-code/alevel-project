package com.alevel.module3.secondmode;

import com.alevel.module3.firstmode.UserWizard;
import com.alevel.module3.firstmode.entity.Operation;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DatabaseManipTest {

    @Test
    void tryToConnect() {
        assertEquals("Failed while authorisation, invalid login or password", assertThrows(RuntimeException.class,
                () -> new DatabaseManip("invalid", "invalid", "invalid", Instant.MIN, Instant.MAX)).getMessage());

        assertEquals("Invalid data parameters!", assertThrows(RuntimeException.class,
                () -> new DatabaseManip("postgres", "password", "mymail@gmail.com",
                        Instant.now(), Instant.now().minusSeconds(10))).getMessage());

        assertDoesNotThrow(() -> new DatabaseManip("postgres", "password", "mymail@gmail.com",
                Instant.MIN, Instant.MAX).close());
    }

    @Test
    void getResults() {
        DatabaseManip emptyResultList = new DatabaseManip("postgres", "password", "mymail@gmail.com",
                Instant.now().minusSeconds(10), Instant.now());

        assertTrue(emptyResultList.getResults().isEmpty());

        UserWizard userWizard = new UserWizard("postgres", "password", "mymail@gmail.com");
        userWizard.newOperation(0, 10, "test");
        userWizard.confirmOperation(0);


        DatabaseManip nonEmptyResultList = new DatabaseManip("postgres", "password", "mymail@gmail.com",
                Instant.now().minusSeconds(10), Instant.now());

        assertFalse(nonEmptyResultList.getResults().isEmpty());

        assertEquals(10, nonEmptyResultList.getResults().get(0).getCost());
        assertEquals(10, nonEmptyResultList.getResults().get(0).getBalance());
        assertEquals(10, nonEmptyResultList.getResults().get(0).getSum());

        /* removing test operation */
        List<Operation> operationList = userWizard.getAccounts().get(0).getOperations();
        userWizard.getAccounts().get(0).getOperations().remove(operationList.size() - 1);
        userWizard.save();
        userWizard.close();

        DatabaseManip emptyResultListPart2 = new DatabaseManip("postgres", "password", "mymail@gmail.com",
                Instant.now().minusSeconds(10), Instant.now());

        assertTrue(emptyResultListPart2.getResults().isEmpty());
        emptyResultList.close();
        emptyResultListPart2.close();
        nonEmptyResultList.close();
    }
}