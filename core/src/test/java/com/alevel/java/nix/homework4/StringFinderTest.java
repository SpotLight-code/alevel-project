package com.alevel.java.nix.homework4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StringFinderTest {

    @Test
    void lengthOfLongestSubstring() {
        StringFinder stringFinder = new StringFinder();

        assertEquals(3,stringFinder.lengthOfLongestSubstring("abcabcbb"));
        assertEquals(0,stringFinder.lengthOfLongestSubstring(""));
        assertEquals(1,stringFinder.lengthOfLongestSubstring(" "));
        assertEquals(4,stringFinder.lengthOfLongestSubstring("abcdabd"));
        assertEquals(4,stringFinder.lengthOfLongestSubstring("abcdda"));
        assertEquals(1,stringFinder.lengthOfLongestSubstring("aaaaaaaaaaaaaaaaaaaa"));
        assertEquals(2,stringFinder.lengthOfLongestSubstring("bababababa"));
        assertEquals(5,stringFinder.lengthOfLongestSubstring("efrftgvfd"));
        assertEquals(12,stringFinder.lengthOfLongestSubstring("abcaftghyujki"));
    }
}