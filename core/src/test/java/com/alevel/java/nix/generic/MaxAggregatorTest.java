package com.alevel.java.nix.generic;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MaxAggregatorTest {

    @Test
    void aggregate() {
        compare(4, 3, 2, 1, 4, 0, -1);
        compare("x", "abs", "bds", "gtrdfa", "dwerfreg", "frefer", "x");

    }

    @SafeVarargs
    final <T extends Comparable<T>> void compare(T res, T... elements) {
        assertDoesNotThrow(() -> assertEquals(res, new MaxAggregator<T>().aggregate(elements).get()));
    }

}