package com.alevel.java.nix.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

@WebServlet(name = "Logger", urlPatterns = "/")
public class IPCollector extends HttpServlet {
    private final static ConcurrentMap<String, String> map = new ConcurrentHashMap<>();
    private static final Logger log = LoggerFactory.getLogger(IPCollector.class);
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String user = req.getHeader("User-Agent");
        String host = req.getRemoteHost();

        log.info("new connection");
        log.info(host);
        log.info(user);


        map.putIfAbsent(host, user);

        var stream = resp.getWriter();
        map.forEach((ip, value) -> {
            String scope = ip.equals(host) ? "<p><b>%s :: %s</b></p>" : "<p>%s :: %s</p>";
            stream.println(String.format(scope, ip, value));
        });
    }
}
