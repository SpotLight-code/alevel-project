package com.alevel.java.nix.homework9;
@FunctionalInterface
public interface Block {
    void run() throws Exception;
}
