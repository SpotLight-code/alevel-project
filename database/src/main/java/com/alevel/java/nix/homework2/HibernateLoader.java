package com.alevel.java.nix.homework2;

import com.alevel.java.nix.homework2.entity.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.List;

public class HibernateLoader {
    private static final Logger log = LoggerFactory.getLogger(HibernateLoader.class);

    public static List<Task> loadUnresolvedTasks(Session session) {
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<Task> criteria = builder.createQuery(Task.class);
        Root<Task> root =  criteria.from(Task.class);

        criteria.where(root.join("foundRoute", JoinType.LEFT).isNull(),
                root.join("impossibleRoute", JoinType.LEFT).isNull());

        return session.createQuery(criteria).getResultList();
    }

    private void resolveAllTasks() {
        Configuration cfg = new Configuration().configure();
        try (SessionFactory factory = cfg.buildSessionFactory();
             Session session = factory.openSession()) {

            try {
                session.beginTransaction();
                int i = 0;
                for (Task task : loadUnresolvedTasks(session)) {
                    int result = new ProblemFinder(task).findMinPath();
                    session.save(result == Integer.MAX_VALUE ? new ImpossibleRoute(task) : new FoundRoute(task, result));
                    i++;
                }
                log.info("Resolved : {} tasks.", i);
            } catch (Exception e) {
                session.getTransaction().rollback();
                log.error("Error happened rollback", e);
            }
        }
    }


    private void loadTasksIntoDatabase(String path) {
        Loader loader = new Loader(path);
        Configuration cfg = new Configuration().configure();
        try (SessionFactory factory = cfg.buildSessionFactory();
             Session session = factory.openSession()) {
            try {
                session.beginTransaction();
                loader.getTasks().forEach(session::save);
                session.getTransaction().commit();
            }catch (Exception e){
                session.getTransaction().rollback();
                log.error("Error uploading info");
                throw new RuntimeException(e);
            }
        }
    }

    public static void main(String[] args) {
        HibernateLoader main = new HibernateLoader();
        main.loadTasksIntoDatabase("database/input.txt");
        main.resolveAllTasks();

    }


}
