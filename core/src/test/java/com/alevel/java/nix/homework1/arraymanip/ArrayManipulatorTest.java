package com.alevel.java.nix.homework1.arraymanip;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArrayManipulatorTest {

    @Test
    void testGeneratingArray() {
        int[][] arrayForTest = {
                {2, 3, 1, 5, 4},
                {2, 3, 7, 2, 1, 2},
                {23, 0, 20, 23, 50, 43},
                {44, 6, 5, 4, 3, 56},
                {10, 0, 0, 13, 0, 12, 0}
        };

        int[][] expected = {
                null,
                {2, 3, 7, 2, 1, 2},
                {0, 20, 50},
                {6, 3},
                {0, 0, 0, 12, 0}
        };

        ArrayManipulator arrayManipulator = new ArrayManipulator();

        assertNull(arrayManipulator.generateArray(arrayForTest[0], 0));

        for (int i = 1; i < expected.length; i++) {
            int[] actual = arrayManipulator.generateArray(arrayForTest[i], i);
            assertNotNull(actual);
            assertArrayEquals(actual, expected[i]);
        }
    }

}