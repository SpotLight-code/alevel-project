create table connection
(
    cost   integer not null,
    "from" integer not null
        constraint connection_from_fkey
            references city,
    "to"   integer not null
        constraint connection_to_fkey
            references city,
    id     serial  not null
        constraint connection_pkey
            primary key
);

INSERT INTO public.connection (cost, from_id, to_id, id) VALUES (1, 1, 2, 1);
INSERT INTO public.connection (cost, from_id, to_id, id) VALUES (1, 3, 2, 7);
INSERT INTO public.connection (cost, from_id, to_id, id) VALUES (4, 3, 1, 6);
INSERT INTO public.connection (cost, from_id, to_id, id) VALUES (1, 2, 3, 4);
INSERT INTO public.connection (cost, from_id, to_id, id) VALUES (3, 1, 3, 2);
INSERT INTO public.connection (cost, from_id, to_id, id) VALUES (3, 5, 3, 11);
INSERT INTO public.connection (cost, from_id, to_id, id) VALUES (1, 2, 1, 3);
INSERT INTO public.connection (cost, from_id, to_id, id) VALUES (4, 2, 4, 5);
INSERT INTO public.connection (cost, from_id, to_id, id) VALUES (4, 4, 2, 9);
INSERT INTO public.connection (cost, from_id, to_id, id) VALUES (1, 3, 4, 8);
INSERT INTO public.connection (cost, from_id, to_id, id) VALUES (1, 4, 3, 10);