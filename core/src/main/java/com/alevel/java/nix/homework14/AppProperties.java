package com.alevel.java.nix.homework14;

public class AppProperties {
    @PropertyKey(key = "test")
    int test;

    public int getTest() {
        return test;
    }
}
