package com.alevel.java.nix.homework2.numberwizard;

public class NumberOperations {

    public void fizzBuzzRightToLeft(int number) {
        if (number == 0) {
            System.out.println("fizzbuzz");
            return;
        }

        StringBuilder stringBuilder = new StringBuilder();

        while (number != 0) {
            boolean flag = false;
            if ((number % 10) % 2 == 0) {
                stringBuilder.append("fizz");
                flag = true;
            }
            if ((number % 10) % 3 == 0) {
                stringBuilder.append("buzz");
                flag = true;
            }
            if (flag) {
                stringBuilder.append("\n");
            }
            number /= 10;
        }

        System.out.print(stringBuilder.toString());
    }

    public void fizzBuzzLeftToRight(int number) {
        if (number == 0) {
            System.out.println("fizzbuzz");
            return;
        }
        int temp = number;
        int count = 1;

        while (temp != 0) {
            count *= 10;
            temp /= 10;
        }

        count /= 10;
        StringBuilder stringBuilder = new StringBuilder();

        for (; count != 0; count /= 10) {
            boolean flag = false;
            if ((number / count) % 2 == 0) {
                stringBuilder.append("fizz");
                flag = true;
            }
            if ((number / count) % 3 == 0) {
                stringBuilder.append("buzz");
                flag = true;
            }
            if (flag) {
                stringBuilder.append("\n");
            }
            number %= count;
        }
        System.out.print(stringBuilder.toString());
    }

    public void fizzBuzzLeftToRightAnother(int number) {
        if (number == 0) {
            System.out.println("fizzbuzz");
            return;
        }
        int inverseNumber = 0;
        int countOfLastZeroValue = 0;
        boolean isZeroEnd = false;

        while (number != 0) {
            if (number % 10 == 0 && !isZeroEnd) {
                countOfLastZeroValue++;
                number /= 10;
                continue;
            }
            isZeroEnd = true;
            inverseNumber += number % 10;
            inverseNumber *= 10;
            number /= 10;
        }

        inverseNumber /= 10;

        fizzBuzzRightToLeft(inverseNumber);

        for (int i = 0; i < countOfLastZeroValue; i++) {
            System.out.println("fizzbuzz");
        }
    }
}
