package com.alevel.java.nix.homework13;
@FunctionalInterface
public interface TripleConsumer<A, B, C>  {
    void consume(A a, B b, C c) throws RuntimeException, IllegalAccessException;
}
