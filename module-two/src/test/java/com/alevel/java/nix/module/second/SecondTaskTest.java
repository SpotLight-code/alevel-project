package com.alevel.java.nix.module.second;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class SecondTaskTest {

    @Test
    void findFirstUnique() {
        assertDoesNotThrow(() -> test("1", "1", "2", "3", "4", "5"));
        assertDoesNotThrow(() -> test("2", "1", "1", "2", "4", "5"));
        assertDoesNotThrow(() -> test("2", "1", "1", "1", "2", "5"));
        assertThrows(Exception.class, () -> test("", "1", "1", "2", "2"));
        assertThrows(Exception.class, () -> test("", ""));
    }

    void test(String res, String... val) throws Exception {
        assertEquals(res, SecondTask.findFirstUnique(Arrays.asList(val)));
    }
}