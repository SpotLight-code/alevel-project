package com.alevel.java.nix.homework13;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CSVMapper<T> {
    private static final Map<Class<?>, TripleConsumer<Field, String, Object>> map = Map.ofEntries(
            Map.entry(int.class, (field, string, object) -> field.setInt(object, Integer.parseInt(string))),
            Map.entry(float.class, (field, string, object) -> field.setFloat(object, Float.parseFloat(string))),
            Map.entry(double.class, (field, string, object) -> field.setDouble(object, Double.parseDouble(string))),
            Map.entry(boolean.class, (field, string, object) -> field.setBoolean(object, Boolean.parseBoolean(string))),
            Map.entry(byte.class, (field, string, object) -> field.setByte(object, Byte.parseByte(string))),
            Map.entry(String.class, (field, string, object) -> field.set(object, string)),
            Map.entry(long.class, (field, string, object) -> field.setLong(object, Long.parseLong(string))),
            Map.entry(char.class, (field, string, object) -> field.setChar(object, string.charAt(0))));

    private final Class<T> type;
    private final Struct struct;

    public CSVMapper(Struct struct, Class<T> type) {
        this.struct = struct;
        this.type = type;
    }


    public static void main(String[] args) {
        System.out.println(new CSVMapper<>(Struct.load("core/example.csv"), Example.class).getList());
    }

    public List<T> getList() {
        List<T> result = new ArrayList<>(struct.getSize());
        try {
            for (int i = 0, size = struct.getSize(); i < size; i++) {
                T instance = type.getConstructor().newInstance();
                for (Field field : type.getDeclaredFields()) {
                    if (field.isAnnotationPresent(CSVName.class)) {
                        try {
                            boolean flag = field.canAccess(instance);
                            if (field.trySetAccessible()) {
                                String data = struct.getValue(i, field.getAnnotation(CSVName.class).key());
                                map.get(field.getType()).consume(field, data, instance);
                                field.setAccessible(flag);
                            }
                        } catch (SecurityException ignored) {
                        }
                    }
                }
                result.add(instance);
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return result;
    }

}
