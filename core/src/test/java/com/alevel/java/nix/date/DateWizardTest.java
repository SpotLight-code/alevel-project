package com.alevel.java.nix.date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

import static com.alevel.java.nix.date.DateWizard.*;
import static org.junit.jupiter.api.Assertions.*;

class DateWizardTest {
    ArrayList<LocalDateTime> list = new ArrayList<>();
    LocalDateTime t = LocalDateTime.now();

    @BeforeEach
    void init(){
        list.add(t.plusHours(2));
        list.add(t);
        list.add(t.plusHours(1));
        list.add(t.plusDays(1));
    }

    @Test
    void groupUpTest() {
        SortedMap<LocalDate, SortedSet<LocalTime>> res = groupUp(list);

        var temp = res.get(t.toLocalDate());

        assertEquals(temp.first(), t.toLocalTime());
        assertEquals(temp.last(), t.plusHours(2).toLocalTime());

        temp = res.get(t.plusDays(1).toLocalDate());
        assertEquals(temp.first(),t.toLocalTime());
    }

    @Test
    void getMaxDurationStreamTest() {
        assertEquals(Duration.ofDays(1), getMaxDurationStream(list));
        assertEquals(Duration.ZERO, getMaxDurationStream(new ArrayList<>()));
    }

    @Test
    void getMaxDurationTest() {
        assertEquals(Duration.ofDays(1), getMaxDuration(list));
        assertEquals(Duration.ZERO, getMaxDuration(new ArrayList<>()));
    }
}