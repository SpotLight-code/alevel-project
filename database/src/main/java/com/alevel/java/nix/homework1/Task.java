package com.alevel.java.nix.homework1;

import java.util.List;
import java.util.Objects;

public class Task {
    private final int id;
    private final City from;
    private final City to;

    public Task(int id, City from, City to) {
        this.id = id;
        this.from = from;
        this.to = to;
    }

    public int resolveProblem(List<City> arr){
        return from.findMinPath(arr, to.getId());
    }

    public int getId() {
        return id;
    }

    public City getFrom() {
        return from;
    }

    public City getTo() {
        return to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                from.equals(task.from) &&
                to.equals(task.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, from, to);
    }
}
