package com.alevel.java.nix.homework3;

public class Transpose {

    private int[][] transposedMatrix;

    public void transposeMatrix(int[][] array) {

        if(array.length == 0 || array[0].length == 0){
            return;
        }

        transposedMatrix = new int[array[0].length][];

        for (int i = 0, lenI = transposedMatrix.length; i < lenI; i++) {
            transposedMatrix[i] = new int[array.length];
            for (int j = 0, lenJ = transposedMatrix[j].length; j < lenJ; j++) {
                transposedMatrix[i][j] = array[j][i];
            }
        }
    }

    public int[][] getTransposedMatrix() {
        return transposedMatrix;
    }
}
