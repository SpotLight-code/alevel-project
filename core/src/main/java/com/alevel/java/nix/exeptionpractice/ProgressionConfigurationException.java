package com.alevel.java.nix.exeptionpractice;

public class ProgressionConfigurationException extends Exception {
    public ProgressionConfigurationException() {
        super();
    }

    public ProgressionConfigurationException(String message) {
        super(message);
    }

    public ProgressionConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProgressionConfigurationException(Throwable cause) {
        super(cause);
    }

    protected ProgressionConfigurationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
