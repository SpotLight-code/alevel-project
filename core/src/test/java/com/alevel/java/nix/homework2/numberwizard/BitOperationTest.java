package com.alevel.java.nix.homework2.numberwizard;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BitOperationTest {

    @Test
    void getCountNonZeroBitsInNumber(){
        BitOperation bitOperation = new BitOperation();

        assertEquals(1, bitOperation.getCountNonZeroBitsInNumber(1));
        assertEquals(64, bitOperation.getCountNonZeroBitsInNumber(-1));
        assertEquals(1, bitOperation.getCountNonZeroBitsInNumber(256));
        assertEquals(1, bitOperation.getCountNonZeroBitsInNumber(1024));
        assertEquals(63, bitOperation.getCountNonZeroBitsInNumber(-2));
        assertEquals(2, bitOperation.getCountNonZeroBitsInNumber(5));
        assertEquals(8, bitOperation.getCountNonZeroBitsInNumber(255));
        assertEquals(0, bitOperation.getCountNonZeroBitsInNumber(0));
        assertEquals(58, bitOperation.getCountNonZeroBitsInNumber(-5325));
        assertEquals(7, bitOperation.getCountNonZeroBitsInNumber(5325));
    }

}