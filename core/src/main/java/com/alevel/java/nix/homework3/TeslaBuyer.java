package com.alevel.java.nix.homework3;

import java.util.Optional;

public class TeslaBuyer {
    private double currentMin;
    private double currentMax;

    public void setUpValues(double... array) throws Exception {

        if (array.length == 0){
            currentMin = currentMax = 0;
            throw new Exception("Array length can't be 0");
        }

        double newMin = 0;
        currentMax = currentMin = array[0];

        for (double value : array) {

            if (value <= 0) {
                throw new Exception("Value can't be 0 or less");
            }

            if (currentMax - currentMin < value - (newMin == 0 ? currentMin : newMin)) {
                currentMax = value;
                if (newMin != 0) {
                    currentMin = newMin;
                    newMin = 0;
                }
            }

            if (currentMin > value && (newMin == 0 || newMin > value)) {
                newMin = value;
            }
        }
    }

    @Override
    public String toString() {
        return "Income = " + (currentMax - currentMin);
    }

    public double getMaxIncome() {
        return currentMax - currentMin;
    }

    public Object getCurrentMin() {
        return Optional.empty();
    }

    public double getCurrentMax() {
        return currentMax;
    }
}
