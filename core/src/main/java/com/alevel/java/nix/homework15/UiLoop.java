package com.alevel.java.nix.homework15;

import java.util.Scanner;

public class UiLoop {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        MultiThreadAppender appender = new MultiThreadAppender();
        String input = "";
        do {
            System.out.print("Input string: ");
            input = scanner.next();
            input += scanner.nextLine();

            appender.append(input);
        } while (!input.equals("quit"));
    }
}
