package com.alevel.java.nix.homework4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ZigZagTest {

    @Test
    void convert() {
        ZigZag zigZag = new ZigZag();

        assertEquals("PAHNAPLSIIGYIR", zigZag.convert("PAYPALISHIRING", 3));
        assertEquals("PINALSIGYAHRPI", zigZag.convert("PAYPALISHIRING", 4));
        assertEquals("PAYPALISHIRING", zigZag.convert("PAYPALISHIRING", 1));
        assertEquals("PYAIHRNAPLSIIG", zigZag.convert("PAYPALISHIRING", 2));
        assertEquals("PHASIYIRPLIGAN", zigZag.convert("PAYPALISHIRING", 5));
        assertEquals("1234567890", zigZag.convert("1234567890", 1));
        assertEquals("PRAIIYHNPSGAIL", zigZag.convert("PAYPALISHIRING", 6));



    }
}