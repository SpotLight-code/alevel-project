package com.alevel.java.nix.homework8.aggregation;

public interface Substance {
    State heatUp(final double t);
    double getTemperature();
    State getState();
}
