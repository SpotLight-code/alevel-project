package com.alevel.java.nix.homework7;

public class LongestPrefix {
    public String longestCommonPrefix(String... strs) { //runtime 0 ms
        if (strs.length == 0) {
            return "";
        }
        char[] common = strs[0].toCharArray();
        int currentMax = common.length;

        for (String s : strs) {
            int len = s.length();
            if (len == 0) {
                return "";
            }
            for (int i = 0, bound = Math.min(currentMax, len); i < bound; i++) {
                if (common[i] != s.charAt(i)) {
                    currentMax = i;
                    if (i == 0) {
                        return "";
                    }
                    break;
                }
                if (i == bound - 1) {
                    currentMax = bound;
                }
            }
        }

        return String.valueOf(common, 0, currentMax);
    }
}