package com.alevel.java.nix.filestream;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Counter {

    public static void main(String[] args) {
        System.out.println(countAllSymbols("core/file.csv"));
    }


    public static Map<Character, Integer> countAllSymbols(String path) {

        Map<Character, Integer> res = new HashMap<>();

        try {
            BufferedReader input = new BufferedReader(new FileReader(path));

            for (String line = input.readLine(); line != null; line = input.readLine()) {
                for (char c : line.toCharArray()) {
                    res.merge(c, 1, Integer::sum);
                }
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        return res;
    }
}
