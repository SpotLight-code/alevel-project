package com.alevel.java.nix.practice1;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class DateWizardTest {

    @Test
    void getAllHolidayDays() {
        List<LocalDate> localDates =
                DateWizard.getAllHolidayDays(LocalDate.of(2020,4,2),
                        LocalDate.of(2020,4,9));

        assertEquals(2, localDates.size());
        assertEquals(LocalDate.of(2020,4,4), localDates.get(0));
        assertEquals(LocalDate.of(2020,4,5), localDates.get(1));
    }
}