package com.alevel.java.nix.homework6.model;

public interface TicTacToe {
    MoveStatus move(int place) throws Exception;
    void reset();
    char whoNowMove();
    String toString();
}
