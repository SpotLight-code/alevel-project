package com.alevel.java.nix.practice1.gallows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GallowsGameTest {


    @Test
    void GameTest() {
        Gallows gallows = new GallowsGame("abcd");

        assertEquals(5, gallows.getChancesCount());

        assertEquals("****", gallows.showWord());
        assertFalse(gallows.isGameFinished());
        assertEquals(5, gallows.getChancesCount());

        assertEquals("a***", gallows.makeMove('a').getCurrentWord());
        assertFalse(gallows.isGameFinished());
        assertEquals(5, gallows.getChancesCount());

        assertEquals("ab**", gallows.makeMove('b').getCurrentWord());
        assertFalse(gallows.isGameFinished());
        assertEquals(5, gallows.getChancesCount());

        assertEquals("ab**", gallows.makeMove('b').getCurrentWord());
        assertFalse(gallows.isGameFinished());
        assertEquals(4, gallows.getChancesCount());

        assertEquals("abc*", gallows.makeMove('c').getCurrentWord());
        assertFalse(gallows.isGameFinished());
        assertEquals(4, gallows.getChancesCount());

        assertEquals("abcd", gallows.makeMove('d').getCurrentWord());
        assertEquals(4, gallows.getChancesCount());
        assertTrue(gallows.isGameFinished());

        assertThrows(RuntimeException.class, () -> gallows.makeMove(' '));

    }
}