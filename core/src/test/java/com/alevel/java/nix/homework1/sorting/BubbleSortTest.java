package com.alevel.java.nix.homework1.sorting;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BubbleSortTest {

    @Test
    void testSort() {
        int[][] sortedArray = {
                {1, 2, 3, 4, 5},
                {1, 2, 2, 2, 3, 7},
                {0, 20, 23, 23, 43, 50},
                {3, 4, 5, 6, 44, 56},
                {0, 0, 0, 0, 10, 12, 13}
        };

        int[][] arrayForSort = {
                {2, 3, 1, 5, 4},
                {2, 3, 7, 2, 1, 2},
                {23, 0, 20, 23, 50, 43},
                {44, 6, 5, 4, 3, 56},
                {10, 0, 0, 13, 0, 12, 0}
        };

        BubbleSort bubbleSort = new BubbleSort();
        for (int i = 0; i < sortedArray.length; i++) {
            bubbleSort.sortArray(arrayForSort[i]);
            assertArrayEquals(sortedArray[i], arrayForSort[i]);
        }

    }


}