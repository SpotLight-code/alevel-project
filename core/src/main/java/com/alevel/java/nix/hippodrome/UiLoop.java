package com.alevel.java.nix.hippodrome;

import java.util.Scanner;

public class UiLoop {
    public static void main(String[] args) {
        int countHorses;
        int probableHorse;
        Scanner scanner = new Scanner(System.in);

        while (true){
            try {
                System.out.print("Enter count of horses (Enter 0 for finish): ");
                countHorses = scanner.nextInt();
                if (countHorses == 0) {
                    break;
                }
                System.out.print("Enter probable win horse: ");
                probableHorse = scanner.nextInt();

                System.out.println();

                Race r = new Race(countHorses);
                System.out.println("Your horse arrived: " + r.startRiding(probableHorse));

                System.out.println(System.lineSeparator() + "All horses results");
                r.getSortedFinishedList().forEach(System.out::println);
                System.out.println();
            }catch (Exception e){
                System.out.println(e.getMessage());
                if(scanner.hasNext()){
                    scanner.nextLine();
                }
            }
        }

    }
}
