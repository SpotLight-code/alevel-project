package com.alevel.java.nix.homework12;

import org.junit.jupiter.api.Test;

import java.io.File;

import static com.alevel.java.nix.homework12.DirWizard.copyDirectory;
import static org.junit.jupiter.api.Assertions.*;

class DirWizardTest {

    @Test
    void copyDirectoryTest() {
        assertTrue(new File("test/forCopy/dir").mkdirs());
        assertDoesNotThrow(()-> assertTrue(new File("test/forCopy/dir/test.txt").createNewFile()));

        File file = new File("forCopy");
        assertFalse(file.exists());
        assertDoesNotThrow(()-> copyDirectory("test/forCopy"));
        assertTrue(file.exists());
        assertTrue(new File("forCopy/dir/test.txt").exists());

        assertTrue(new File("forCopy/dir/test.txt").delete());
        assertTrue(new File("forCopy/dir").delete());
        assertTrue(new File("forCopy").delete());
        assertTrue(new File("test/forCopy/dir/test.txt").delete());
        assertTrue(new File("test/forCopy/dir").delete());
        assertTrue(new File("test/forCopy").delete());
        assertTrue(new File("test").delete());
    }
}