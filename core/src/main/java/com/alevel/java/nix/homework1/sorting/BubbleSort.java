package com.alevel.java.nix.homework1.sorting;

public class BubbleSort {

    public void sortArray(int[] array) {
        boolean flag = true;

        while (flag) {
            flag = false;

            for (int i = 1; i < array.length; i++) {
                if (array[i - 1] > array[i]) {
                    flag = true;
                    var temp = array[i];
                    array[i] = array[i - 1];
                    array[i - 1] = temp;
                }
            }
        }
    }
}