package com.alevel.java.nix.homework8.universe;

public class StudentContract extends Student {

    private double cost;

    public double getCost() {
        return cost;
    }

    public void setCost(final double cost) {
        this.cost = cost;
    }

    public StudentContract(final String name, final int age, final double cost) {
        super(name, age);
        this.cost = cost;
    }

    @Override
    public String toString(){
        return super.toString() + String.format(" contract cost: %.2f", getCost());
    }

    //return String.format("Name: %s, age: %d, contract cost: %.2f", getName(), getAge(), getCost());
    // what better?
}
