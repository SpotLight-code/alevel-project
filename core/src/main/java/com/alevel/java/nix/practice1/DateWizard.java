package com.alevel.java.nix.practice1;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class DateWizard {

    public static List<LocalDate> getAllHolidayDays(LocalDate localDate1, LocalDate localDate2) {
        if (localDate1.compareTo(localDate2) > 0) {
            return getAllHolidayDays(localDate2, localDate1);
        }
        return localDate1.datesUntil(localDate2)
                .filter((date) -> date.getDayOfWeek() == DayOfWeek.SATURDAY || date.getDayOfWeek() == DayOfWeek.SUNDAY)
                .collect(Collectors.toList());
    }
}
