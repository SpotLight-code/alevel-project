package com.alevel.java.nix;

import com.alevel.java.nix.homework1.City;
import com.alevel.java.nix.homework1.JDBCRunner;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class JDBCRunnerTest {

    @Test
    void testDatabase() {

        assertDoesNotThrow(() -> {
            JDBCRunner jdbcRunner = new JDBCRunner();

            List<City> list = new ArrayList<>();
            list.add(new City(1, "gdansk"));
            list.add(new City(2, "bydgoszcz"));
            list.add(new City(3, "torun"));
            list.add(new City(4, "warszawa"));
            list.add(new City(5, "kharkiv"));

            assertEquals(list, jdbcRunner.getCities());

            List<Map<Integer, Integer>> maps = new ArrayList<>(5);
            for (int i = 0; i < 5; i++) {
                maps.add(new HashMap<>());
            }
            maps.get(0).put(2,1);
            maps.get(0).put(3,3);
            maps.get(1).put(1,1);
            maps.get(1).put(3,1);
            maps.get(1).put(4,4);
            maps.get(2).put(1,4);
            maps.get(2).put(2,1);
            maps.get(2).put(4,1);
            maps.get(3).put(2,4);
            maps.get(3).put(3,1);
            maps.get(4).put(3,3);

            for (int i = 0; i < 5; i++) {
                assertEquals(maps.get(i), jdbcRunner.getAllNeighborsByCityId(i+1));
            }
            jdbcRunner.close();

        });
    }

}