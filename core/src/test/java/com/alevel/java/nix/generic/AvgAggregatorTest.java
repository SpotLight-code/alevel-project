package com.alevel.java.nix.generic;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AvgAggregatorTest {

    @Test
    void aggregate() {
        compare(5.5, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        compare(2.3, 2.3, 2.3, 2.3, 2.3);
    }

    @SafeVarargs
    final <T extends Number> void compare(double res, T... elements) {
        assertEquals(res, new AvgAggregator<T>().aggregate(elements), 0.01);

    }
}