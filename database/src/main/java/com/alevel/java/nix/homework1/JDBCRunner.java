package com.alevel.java.nix.homework1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Closeable;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class JDBCRunner implements Closeable {
    private Connection connection;
    private static final Logger log = LoggerFactory.getLogger(JDBCRunner.class);

    public JDBCRunner(String propertyFile) throws SQLException {
        init(propertyFile);
    }

    public JDBCRunner() throws SQLException {
        init("dbconfig.properties");
    }

    private void init(String propertyFile) throws SQLException {
        try(var input = JDBCRunner.class.getClassLoader().getResourceAsStream(propertyFile)) {
            Properties properties = new Properties();
            properties.load(input);
            connection = DriverManager.getConnection(properties.getProperty("host"), properties);
            log.info("Connection successful");
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
    public List<City> getCities() {
        try (var ps = connection.prepareStatement("select id, name from City")) {
            List<City> result = new ArrayList<>();
            var res = ps.executeQuery();
            while (res.next()) {
                result.add(new City(res.getInt("id"), res.getString("name")));
            }
            return result;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Map<Integer, Integer> getAllNeighborsByCityId(int cityId) {
        try (var st = connection.prepareStatement("select \"cost\", to_id from Connection where from_id = ?")) {
            st.setInt(1, cityId);

            ResultSet resultSet = st.executeQuery();

            Map<Integer, Integer> map = new HashMap<>();
            while (resultSet.next()) {
                map.put(resultSet.getInt("to"), resultSet.getInt("cost"));
            }
            return map;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() {
        try {
            connection.close();
            log.info("Successfully closed");
        } catch (SQLException e) {
            log.error("Bad close", e);
        }
    }

    public List<Task> getTasks(List<City> cities) {

        try(var st = connection.prepareStatement("select \"id\", from_id, to_id from Problems")) {
            List<Task> list = new ArrayList<>();
            var res = st.executeQuery();

            while (res.next()){
                list.add(new Task(
                        res.getInt("id"),
                        cities.get(res.getInt("from")-1),
                        cities.get(res.getInt("to")-1)));
            }

            return list;
        }catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void resolveTask(Task task, int cost) {
        try(var st = connection.prepareStatement("INSERT INTO FoundRoutes values (?, ?)")){
            st.setInt(1, task.getId());
            st.setInt(2, cost);
            st.executeUpdate();
        }catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    public void unreachableTask(Task task) {
        try(var st = connection.prepareStatement("INSERT INTO ImpossibleRoutes values (?)")){
            st.setInt(1, task.getId());
            st.executeUpdate();
        }catch (SQLException e){
            throw new RuntimeException(e);
        }
    }
}
