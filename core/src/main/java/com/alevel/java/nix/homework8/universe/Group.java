package com.alevel.java.nix.homework8.universe;

public class Group {
    final private Student[] students;

    public Group(final Student... students) {
        this.students = students;
    }

    public Student[] getStudents() {
        return students;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Student s : students){
            stringBuilder.append(s).append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }


    public static void main(String[] args) {
        Group group = new Group(
                new Student("Alex", 19),
                new StudentContract("Paul", 21, 1830.40),
                new Student("Stephan", 20),
                new StudentContract("Jack", 28, 1933.51),
                new Student("Fill", 21),
                new Student("Deya", 18),
                new Student("Alice", 20)
        );


        System.out.print(group);
    }
}
