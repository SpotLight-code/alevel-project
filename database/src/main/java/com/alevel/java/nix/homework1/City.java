package com.alevel.java.nix.homework1;

import java.util.*;

public final class City {
    final private int id;
    final private String name;
    final private Map<Integer, Integer> neighbors = new HashMap<>();

    public City(int id, String name) {
        this.name = name;
        this.id = id;
    }

    public int findMinPath(List<City> cities, int destination) {
        return findMinPath(cities, new HashSet<>(), destination, 0, Integer.MAX_VALUE);
    }

    private int findMinPath(List<City> cities, Set<Integer> src, int destination, int currentDistance, int currentMin) {
        if (currentDistance >= 200_000 || currentMin < currentDistance) {
            return Integer.MAX_VALUE;
        }
        Set<Integer> allNeighbors = neighbors.keySet();
        int minPath = neighbors.getOrDefault(destination, Integer.MAX_VALUE - currentDistance) + currentDistance;
        src.add(id);
        for (int current : allNeighbors) {
            if (!src.contains(current)) {
                minPath = Math.min(minPath,
                        cities.get(current - 1).
                                findMinPath(cities, src, destination, currentDistance + neighbors.get(current), minPath));

            }
        }
        src.remove(id);
        return minPath;
    }

    public void addNeighbor(int cityId, int cost) {
        neighbors.put(cityId, cost);
    }

    public void addAllNeighbor(Map<Integer, Integer> map) {
        neighbors.putAll(map);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return id == city.id &&
                Objects.equals(name, city.name) &&
                Objects.equals(neighbors, city.neighbors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, neighbors);
    }
}
