package com.alevel.java.nix.generic;

import java.util.StringJoiner;

public class CSVAggregator<T> implements Aggregator<T,String> {

    @Override
    public String aggregate(T[] items) {

        StringJoiner stringJoiner = new StringJoiner(", ");

        for (T t : items) {
            stringJoiner.add(t.toString());
        }

        return stringJoiner.toString();
    }
}
