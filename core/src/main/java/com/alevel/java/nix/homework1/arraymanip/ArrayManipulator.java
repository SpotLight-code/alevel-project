package com.alevel.java.nix.homework1.arraymanip;

public class ArrayManipulator {

    public int[] generateArray(int[] array, int k) {

        if (k == 0) {
            return null;
        }

        int[] result = new int[getCountOfValues(array, k)];

        for (int i = 0, j = 0; i < array.length; i++) {
            if (array[i] % k == 0) {
                result[j++] = array[i];
            }
        }
        return result;
    }

    private int getCountOfValues(int[] array, int k) {
        int count = 0;
        for (int i = 0, j = 0; i < array.length; i++) {
            if (array[i] % k == 0) {
                count++;
            }
        }
        return count;
    }

}