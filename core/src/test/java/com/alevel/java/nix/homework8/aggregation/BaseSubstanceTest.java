package com.alevel.java.nix.homework8.aggregation;

import org.junit.jupiter.api.Test;

import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.*;

class BaseSubstanceTest {

    @Test
    void heatUp() {
        BaseSubstance[] baseSubstance = {new Iron(), new Oxygen(), new Water()};

        State[] states = {State.SOLID, State.VIGOR, State.GAS};

        double absoluteZero = -273.15;

        assertEquals(states[0], baseSubstance[0].heatUp(absoluteZero));
        assertEquals(states[0], baseSubstance[1].heatUp(absoluteZero));
        assertEquals(states[0], baseSubstance[2].heatUp(absoluteZero));
        assertEquals(absoluteZero, baseSubstance[0].getTemperature());
        assertEquals(absoluteZero, baseSubstance[1].getTemperature());
        assertEquals(absoluteZero, baseSubstance[2].getTemperature());

        assertEquals(states[1], baseSubstance[0].heatUp(2000));
        assertEquals(states[1], baseSubstance[1].heatUp(-200));
        assertEquals(states[1], baseSubstance[2].heatUp(50));
        assertEquals(2000, baseSubstance[0].getTemperature());
        assertEquals(-200, baseSubstance[1].getTemperature());
        assertEquals(50, baseSubstance[2].getTemperature());


        assertEquals(states[2], baseSubstance[0].heatUp(Integer.MAX_VALUE));
        assertEquals(states[2], baseSubstance[1].heatUp(Integer.MAX_VALUE));
        assertEquals(states[2], baseSubstance[2].heatUp(Integer.MAX_VALUE));
        assertEquals(Integer.MAX_VALUE, baseSubstance[0].getTemperature());
        assertEquals(Integer.MAX_VALUE, baseSubstance[1].getTemperature());
        assertEquals(Integer.MAX_VALUE, baseSubstance[2].getTemperature());


    }
}