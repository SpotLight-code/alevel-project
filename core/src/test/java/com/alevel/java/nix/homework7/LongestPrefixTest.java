package com.alevel.java.nix.homework7;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LongestPrefixTest {
    LongestPrefix longestPrefix = new LongestPrefix();

    @Test
    void longestCommonPrefix() {
        assertPrefix("fl", "flower", "flfwef", "flefew");
        assertPrefix("", "flower", "", "flowerdsfdsfefw");
        assertPrefix("flower", "flower", "flowerfsdfsd", "flowerfsfsd");
        assertPrefix("", "");
        assertPrefix("a", "absdfr", "aref", "adewfer");
        assertPrefix("", "flower", "dflfwef", "flefew");
    }

    void assertPrefix(String exp, String... strs) {
        assertEquals(exp, longestPrefix.longestCommonPrefix(strs));
    }
}