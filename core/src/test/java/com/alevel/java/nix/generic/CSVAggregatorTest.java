package com.alevel.java.nix.generic;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CSVAggregatorTest {

    @Test
    void aggregate() {
        compare("1, 2, 3, 4, 5", 1, 2, 3, 4, 5);
        compare("");
        compare("a, b, c", "a", "b", "c");
    }


    @SafeVarargs
    final <T> void compare(T res, T... elements) {
        assertEquals(res, new CSVAggregator<T>().aggregate(elements));
    }
}