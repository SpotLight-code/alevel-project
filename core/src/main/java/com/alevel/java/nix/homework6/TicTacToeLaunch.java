package com.alevel.java.nix.homework6;

import com.alevel.java.nix.homework6.controller.MainGameLoop;
import com.alevel.java.nix.homework6.model.TicTacToe;
import com.alevel.java.nix.homework6.model.TicTacToe3_3;

public class TicTacToeLaunch {
    public static void main(String[] args) {
        TicTacToe ticTacToe = new TicTacToe3_3();
        MainGameLoop mainGameLoop = new MainGameLoop(System.out, System.in,ticTacToe);
        while (true){
            mainGameLoop.launchGame();
        }
    }
}
