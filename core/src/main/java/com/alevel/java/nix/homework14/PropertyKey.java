package com.alevel.java.nix.homework14;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(value= RetentionPolicy.RUNTIME)
public @interface PropertyKey {
    String key();
}
