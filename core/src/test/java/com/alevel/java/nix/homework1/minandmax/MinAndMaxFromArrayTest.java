package com.alevel.java.nix.homework1.minandmax;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MinAndMaxFromArrayTest {
    @Test
    void testMinAndMax() {
        int[][] arrayForTest = {
                {2, 3, 1, 5, 4},
                {2, 3, 7, 2, 1, 2},
                {23, 0, 20, 23, 50, 43},
                {44, 6, 5, 4, 3, 56},
                {10, 0, 0, 13, 0, 12, 0}
        };

        int[][] expectedResult = {
                {1, 5},
                {1, 7},
                {0, 50},
                {3, 56},
                {0, 13}
        };

        MinAndMaxFromArray minAndMax = new MinAndMaxFromArray();
        for (int i = 0; i < expectedResult.length; i++) {
            minAndMax.setUpMinAndMaxFromArray(arrayForTest[i]);
            assertEquals(expectedResult[i][0], minAndMax.getMin());
            assertEquals(expectedResult[i][1], minAndMax.getMax());
        }
    }
}