package com.alevel.java.nix.generic;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DistinctAggregatorTest {

    @Test
    void aggregate() {
        compare(4, 1, 2, 3, 4, 4, 3, 2, 1);
        compare(3, "3", "213", "21", "21", "21");
    }

    @SafeVarargs
    final <T> void compare(T res, T... elements) {
        assertEquals(res, new DistinctAggregator<T>().aggregate(elements));
    }
}