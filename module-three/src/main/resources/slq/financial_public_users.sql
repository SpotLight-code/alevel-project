create table users
(
    id    serial       not null
        constraint users_pkey
            primary key,
    email varchar(100) not null
        constraint users_email_key
            unique
);

alter table users
    owner to postgres;

INSERT INTO public.users (id, email) VALUES (0, 'mymail@gmail.com');