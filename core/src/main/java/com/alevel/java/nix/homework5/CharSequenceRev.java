package com.alevel.java.nix.homework5;


public class CharSequenceRev implements CharSequence {
    CharSequence charSequence;

    public static CharSequence reverse(CharSequence e) {
        return new CharSequenceRev(e);
    }

    public CharSequenceRev(CharSequence charSequence) {
        this.charSequence = charSequence;
    }

    @Override
    public String toString() {
        return new StringBuilder(this).toString();
    }

    @Override
    public int length() {
        return charSequence.length();
    }

    @Override
    public char charAt(int i) {
        return charSequence.charAt(length() - 1 - i);
    }

    @Override
    public CharSequence subSequence(int i, int i1) {
        if (i > i1 || i1 >= length() || i < 0) {
            throw new IllegalArgumentException("Illegal argument");
        }

        if (i == i1) {
            return "";
        }
        int len = length();

        return reverse(charSequence.subSequence(len - i1, len - i));
    }
}
