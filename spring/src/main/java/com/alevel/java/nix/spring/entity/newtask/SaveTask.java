package com.alevel.java.nix.spring.entity.newtask;

public class SaveTask {
    private String description;
    private boolean done = false;

    public SaveTask(String description){
        this.description = description;
    }

    public SaveTask(String description, boolean done) {
        this.description = description;
        this.done = done;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }
}
