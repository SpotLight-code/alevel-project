package com.alevel.java.nix.homework8.universe;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GroupTest {
    @Test
    void testOutput() {
        Group group = new Group(
                new Student("Alex", 19),
                new StudentContract("Paul", 21, 1830.40),
                new Student("Stephan", 20),
                new StudentContract("Jack", 28, 1933.51),
                new Student("Fill", 21),
                new Student("Deya", 18),
                new Student("Alice", 20)
        );

        String[] expected = {
                "Name: Alex, age: 19",
                "Name: Paul, age: 21 contract cost: 1830,40",
                "Name: Stephan, age: 20",
                "Name: Jack, age: 28 contract cost: 1933,51",
                "Name: Fill, age: 21",
                "Name: Deya, age: 18",
                "Name: Alice, age: 20"};


        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], group.getStudents()[i].toString());
            if(expected[i].contains("contract")){
                assertTrue(group.getStudents()[i] instanceof StudentContract);
            }else {
                assertFalse(group.getStudents()[i] instanceof StudentContract);
            }
        }

    }

}