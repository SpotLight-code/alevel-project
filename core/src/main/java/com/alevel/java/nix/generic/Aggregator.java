package com.alevel.java.nix.generic;

public interface Aggregator<T, R> {
    R aggregate(T[] items);
}
