create table city
(
    id   integer      not null
        constraint city_pkey
            primary key,
    name varchar(100) not null
        constraint city_name_key
            unique
);


INSERT INTO public.city (id, name) VALUES (1, 'gdansk');
INSERT INTO public.city (id, name) VALUES (2, 'bydgoszcz');
INSERT INTO public.city (id, name) VALUES (3, 'torun');
INSERT INTO public.city (id, name) VALUES (4, 'warszawa');
INSERT INTO public.city (id, name) VALUES (5, 'kharkiv');