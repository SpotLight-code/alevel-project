package com.alevel.java.nix.homework4;

public class StringFinder {

    public int lengthOfLongestSubstring(String s) {

        int longestSubstring = 0, stringLength = s.length();

        char[] ss = s.toCharArray();

        int start = 0;
        for (int i = 0; i < stringLength; i++) {

            start = getMaxStartOfSubstring(ss, ss[i], start, i);

            if (i - start + 1 > longestSubstring) { // index mistake, 2 - 0 = 0, but {0,1,2} - 3 elements.
                longestSubstring = i - start + 1;
            }
        }

        return longestSubstring;
    }

    private int getMaxStartOfSubstring(char[] s, char c, int start, int end) {

        int result = start;
        for (int i = start; i < end; i++) {
            if (s[i] == c) {
                result = i + 1;
                break;
            }
        }
        return result;
    }
}
