package com.alevel.java.nix.homework2.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "connection")
public class Connection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Integer cost;
    @ManyToOne()
    @JoinColumn(name = "from_id")
    private City fromCity;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "to_id")
    private City toCity;

    public Connection(){}

    public Connection(int cost, City fromCity, City toCity) {
        this.cost = cost;
        this.fromCity = fromCity;
        this.toCity = toCity;
    }

    public Integer getCost() {
        return cost;
    }

    public void setCost(Integer cost) {
        this.cost = cost;
    }

    public City getFromCity() {
        return fromCity;
    }

    public void setFromCity(City from) {
        this.fromCity = from;
    }

    public City getToCity() {
        return toCity;
    }

    public void setToCity(City to) {
        this.toCity = to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Connection that = (Connection) o;
        return Objects.equals(cost, that.cost) &&
                fromCity.equals(that.fromCity) &&
                toCity.equals(that.toCity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cost, fromCity, toCity);
    }

    @Override
    public String toString() {
        return "\n\tConnection{" +
                "id=" + id +
                ", cost=" + cost +
                ", from=" + fromCity.getName() +
                ", to=" + toCity.getName() +
                "}";
    }
}
