package com.alevel.java.nix.homework2.numberwizard;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class NumberOperationsTest {

    NumberOperations numberOperations;
    ByteArrayOutputStream out;

    int[] testNumbers = {123456, 0, 157711, 69, -12, -15};

    @BeforeEach
    void init() {
        numberOperations = new NumberOperations();
        out = new ByteArrayOutputStream();
        System.setOut(new PrintStream(out));
    }

    @Test
    void fizzBuzzLeftToRight() {

        String[] expectedOutput = {
                "fizz\nbuzz\nfizz\nfizzbuzz\n",
                "fizzbuzz\n",
                "",
                "fizzbuzz\nbuzz\n",
                "fizz\n",
                ""
        };

        for (int i = 0; i < expectedOutput.length; i++) {
            out.reset();
            numberOperations.fizzBuzzLeftToRight(testNumbers[i]);
            assertEquals(expectedOutput[i], out.toString());
        }
    }

    @Test
    void fizzBuzzRightToLeft() {
        String[] expectedOutput = {
                "fizzbuzz\nfizz\nbuzz\nfizz\n",
                "fizzbuzz\n",
                "",
                "buzz\nfizzbuzz\n",
                "fizz\n",
                ""
        };

        for (int i = 0; i < expectedOutput.length; i++) {
            out.reset();
            numberOperations.fizzBuzzRightToLeft(testNumbers[i]);
            assertEquals(expectedOutput[i], out.toString());
        }

    }

    @Test
    void fizzBuzzLeftToRightAnother() {
        String[] expectedOutput = {
                "fizz\nbuzz\nfizz\nfizzbuzz\n",
                "fizzbuzz\n",
                "",
                "fizzbuzz\nbuzz\n",
                "fizz\n",
                ""
        };

        for (int i = 0; i < expectedOutput.length; i++) {
            out.reset();
            numberOperations.fizzBuzzLeftToRightAnother(testNumbers[i]);
            assertEquals(expectedOutput[i], out.toString());
        }
    }

}