package com.alevel.java.nix.homework1;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class CityWizardTest {
    private static final JDBCRunner database = mock(JDBCRunner.class);
    @BeforeAll
    static void init() throws SQLException {
        List<City> cities = new ArrayList<>(3);
        cities.add(new City(1,"test1"));
        cities.add(new City(2,"test2"));
        cities.add(new City(3,"test3"));

        when(database.getCities()).thenReturn(cities);
        doNothing().when(database).close();
        doNothing().when(database).unreachableTask(any());
        doNothing().when(database).resolveTask(any(), anyInt());

        Map<Integer, Integer> map1 = new HashMap<>();
        map1.put(2,2);
        when(database.getAllNeighborsByCityId(1)).thenReturn(map1);

        Map<Integer, Integer> map2 = new HashMap<>();
        map2.put(1, 2);
        when(database.getAllNeighborsByCityId(2)).thenReturn(map2);


        Map<Integer, Integer> map3 = new HashMap<>();
        map3.put(2, 2);
        map3.put(1, 10);
        when(database.getAllNeighborsByCityId(3)).thenReturn(map3);

        List<Task> tasks = new ArrayList<>();
        tasks.add(new Task(1, cities.get(0), cities.get(2)));
        tasks.add(new Task(2, cities.get(2), cities.get(0)));
        when(database.getTasks(any())).thenReturn(tasks);

    }

    @Test
    void testAlgorithm(){
        CityWizard cityWizard = CityWizard.initFromDatabase(database);

        int[] res = cityWizard.findAllTasks();

        assertEquals(Integer.MAX_VALUE, res[0]);
        assertEquals(4, res[1]);
    }

}