package com.alevel.java.nix.homework2.numberwizard;

public class BitOperation {

    public int getCountNonZeroBitsInNumber(long number) {
        int count = 0;

        while (number != 0) {
            if ((number & 1) == 1) {
                count += 1;
            }
            number >>>= 1;
        }

        return count;
    }

}
