package com.alevel.java.nix.homework6;

import com.alevel.java.nix.homework6.model.MoveStatus;
import com.alevel.java.nix.homework6.model.TicTacToe;
import com.alevel.java.nix.homework6.model.TicTacToe3_3;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TicTacToeTest {

    @Test
    void whoNowMove() {
        TicTacToe ticTacToe = new TicTacToe3_3();

        assertEquals('X', ticTacToe.whoNowMove());
        assertDoesNotThrow(() -> ticTacToe.move(1));
        assertEquals('O', ticTacToe.whoNowMove());
        assertDoesNotThrow(() -> ticTacToe.move(2));
        assertEquals('X', ticTacToe.whoNowMove());
        assertDoesNotThrow(() -> ticTacToe.move(3));
        assertEquals('O', ticTacToe.whoNowMove());
    }

    @Test
    void move() {
        TicTacToe3_3 ticTacToe = new TicTacToe3_3();

        assertDoesNotThrow(() -> ticTacToe.move(1));
        assertThrows(Exception.class, () -> ticTacToe.move(1));
        assertTrue(ticTacToe.isSymbolIsSameWithSymbolInDesk(1,'X'));
        assertThrows(Exception.class, () -> ticTacToe.move(3333));
        assertThrows(Exception.class, () -> ticTacToe.move(0));
    }

    MoveStatus moveStatus;
    @Test
    void simulateDraw() {
        TicTacToe ticTacToe = new TicTacToe3_3();


        assertDoesNotThrow(() -> moveStatus = ticTacToe.move(1));
        assertEquals('\0',moveStatus.getWinner());
        assertFalse(moveStatus.isGameFinished());
        assertDoesNotThrow(() -> moveStatus = ticTacToe.move(2));
        assertEquals('\0',moveStatus.getWinner());
        assertFalse(moveStatus.isGameFinished());
        assertDoesNotThrow(() -> moveStatus = ticTacToe.move(3));
        assertEquals('\0',moveStatus.getWinner());
        assertFalse(moveStatus.isGameFinished());
        assertDoesNotThrow(() -> moveStatus = ticTacToe.move(4));
        assertEquals('\0',moveStatus.getWinner());
        assertFalse(moveStatus.isGameFinished());
        assertDoesNotThrow(() -> moveStatus = ticTacToe.move(6));
        assertEquals('\0',moveStatus.getWinner());
        assertFalse(moveStatus.isGameFinished());
        assertDoesNotThrow(() -> moveStatus = ticTacToe.move(5));
        assertEquals('\0',moveStatus.getWinner());
        assertFalse(moveStatus.isGameFinished());
        assertDoesNotThrow(() -> moveStatus = ticTacToe.move(7));
        assertEquals('\0',moveStatus.getWinner());
        assertFalse(moveStatus.isGameFinished());
        assertDoesNotThrow(() -> moveStatus = ticTacToe.move(9));
        assertEquals('\0',moveStatus.getWinner());
        assertFalse(moveStatus.isGameFinished());
        assertDoesNotThrow(() -> moveStatus = ticTacToe.move(8));

        assertTrue(moveStatus.isGameFinished());
        assertEquals('\0',moveStatus.getWinner());
        assertTrue(moveStatus.isDraw());
    }

    @Test
    void reset() {
        TicTacToe ticTacToe = new TicTacToe3_3();

        assertDoesNotThrow(() -> ticTacToe.move(1));
        ticTacToe.reset();
        assertDoesNotThrow(() -> ticTacToe.move(1));
        ticTacToe.reset();
        /* Simulate win*/
        assertDoesNotThrow(() -> assertFalse(ticTacToe.move(1).isDraw()));
        assertDoesNotThrow(() -> assertFalse(ticTacToe.move(2).isGameFinished()));
        assertDoesNotThrow(() -> assertFalse(ticTacToe.move(5).isGameFinished()));
        assertDoesNotThrow(() -> assertFalse(ticTacToe.move(3).isDraw()));
        assertDoesNotThrow(() -> assertTrue(ticTacToe.move(9).isGameFinished()));


    }
}