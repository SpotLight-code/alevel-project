package com.alevel.java.nix.homework2.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "impossibleroutes")
public class ImpossibleRoute {

    @Id
    private Integer id;

    @OneToOne
    @JoinColumn(name = "id")
    @MapsId
    private Task task;

    public ImpossibleRoute() {
    }

    public ImpossibleRoute(Task task) {
        this.task = task;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImpossibleRoute route = (ImpossibleRoute) o;
        return task.equals(route.task);
    }

    @Override
    public int hashCode() {
        return Objects.hash(task);
    }
}
