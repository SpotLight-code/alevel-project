package com.alevel.java.nix.homework2;

import com.alevel.java.nix.homework2.entity.City;
import com.alevel.java.nix.homework2.entity.Connection;
import com.alevel.java.nix.homework2.entity.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Loader {
    private static final Logger log = LoggerFactory.getLogger(Loader.class);

    private final String path;

    private List<City> cities;
    private List<Connection> connections;
    private List<Task> tasks;

    public Loader(String path) {
        this.path = path;
        collectData();
    }


    private void collectData(){
        try(BufferedReader reader = new BufferedReader(new FileReader(path))){
            int countCity = Integer.parseInt(reader.readLine());

            cities = new ArrayList<>(countCity);
            List<List<String>>  unparsedConnections = new ArrayList<>(countCity);

            Map<String, Integer> cityIndexes = new HashMap<>();

            int totalConnectionCount = 0;
            for (int i = 0; i < countCity; i++) {
                String name = reader.readLine();
                int connectionsCount = Integer.parseInt(reader.readLine());
                totalConnectionCount += connectionsCount;
                cities.add(new City(name));
                cityIndexes.put(name, i);
                unparsedConnections.add(new ArrayList<>(connectionsCount));

                for (int j = 0; j < connectionsCount; j++) {
                    unparsedConnections.get(i).add(reader.readLine());
                }

            }
            connections = new ArrayList<>(totalConnectionCount);
            parseConnections(unparsedConnections);

            log.info("Loaded {} cities", countCity);
            log.info("Loaded {} connections", totalConnectionCount);

            int countProblems = Integer.parseInt(reader.readLine());
            tasks = new ArrayList<>(countProblems);
            for (int i = 0; i < countProblems; i++) {
                String[] fromAndTo = reader.readLine().split(" ");
                Task task = new Task();
                task.setFrom(cities.get(cityIndexes.get(fromAndTo[0])));
                task.setTo(cities.get(cityIndexes.get(fromAndTo[1])));
                tasks.add(task);
            }
            log.info("Loaded {} problems", countProblems);

        } catch (FileNotFoundException e) {
            log.error("Incorrect path to file, current dir" + new File("").getAbsolutePath());
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }


    }

    private void parseConnections(List<List<String>> unparsedConnections) {
        for (int i = 0, size = cities.size(); i < size; i++) {
            City from = cities.get(i);
            for(String s : unparsedConnections.get(i)){
                String[] strs =  s.split(" ");

                Connection connection = new Connection();
                connection.setFromCity(from);
                connection.setToCity(cities.get(Integer.parseInt(strs[0]) - 1));
                connection.setCost(Integer.parseInt(strs[1]));

                from.addConnection(connection);
                connections.add(connection);
            }
        }
    }

    public List<City> getCities() {
        return cities;
    }

    public List<Connection> getConnections() {
        return connections;
    }

    public List<Task> getTasks() {
        return tasks;
    }
}
