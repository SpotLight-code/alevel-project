package com.alevel.java.nix.homework9;

import ch.qos.logback.classic.Logger;
import org.slf4j.LoggerFactory;

public class Retry {
    final private int countOfTries;
    final private int firstTimeSleep;
    final static Logger log = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Retry.class);

    public Retry(int countOfTries, int firstTimeSleep) {

        if (countOfTries <= 0) {
            throw new IllegalArgumentException("Count of tries mustn't be less than 0");
        }
        if (firstTimeSleep <= 0) {
            throw new IllegalArgumentException("First time of sleep can't be less than 0");
        }

        this.countOfTries = countOfTries;
        this.firstTimeSleep = firstTimeSleep;
    }

    public void launch(Block block) throws Exception {
        Exception exception;

        if (countOfTries == 1) {
            try {
                block.run();
                log.info("Single try was successful");
            } catch (Exception e) {
                log.warn("Single try was unsuccessful");
                throw e;
            }
            return;
        }

        try {
            block.run();
            log.info("Success on first try!");
            return;
        }catch (Exception e){
            exception = e;
            log.warn("First try was unsuccessful");
        }

        for (int i = 1; i < countOfTries; i++) {
            try {
                Thread.sleep(firstTimeSleep * i);
                block.run();
                log.info(String.format("Success on %d try", i + 1));
                return;
            } catch (Exception e) {
                exception = e;
                log.warn(String.format("%d try wand unsuccessful, waiting %d ms", i + 1, firstTimeSleep * i));
            }
        }
        throw exception;
    }
}
