package com.alevel.java.nix.module.second;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class SecondTaskTest {
    SecondTask secondTask = new SecondTask();

    @Test
    void maxBinaryTreeCount() {
        TreeNode treeNode = new TreeNode(1);

        treeNode.right = new TreeNode(2);
        treeNode.left = new TreeNode(2);

        treeNode.right.right = new TreeNode(3);
        assertEquals(3, secondTask.getMaxCountOfBinaryTree(treeNode));

        treeNode.right.right.left = new TreeNode(4);
        treeNode.right.right.right = new TreeNode(4);
        assertEquals(4, secondTask.getMaxCountOfBinaryTree(treeNode));

        treeNode.right.right.right.right = new TreeNode(5);
        assertEquals(5, secondTask.getMaxCountOfBinaryTree(treeNode));
        assertEquals(0, secondTask.getMaxCountOfBinaryTree(null));
    }

    @Test
    void isStringValid() {
        assertTrue(secondTask.isStringValid(""));
        assertTrue(secondTask.isStringValid("()"));
        assertTrue(secondTask.isStringValid("({})"));
        assertTrue(secondTask.isStringValid("[{()}]"));
        assertTrue(secondTask.isStringValid("[[]]"));

        assertFalse(secondTask.isStringValid("[[]"));
        assertFalse(secondTask.isStringValid(")("));
        assertFalse(secondTask.isStringValid("{)"));
        assertFalse(secondTask.isStringValid("("));
        assertFalse(secondTask.isStringValid("}"));
        assertFalse(secondTask.isStringValid("(}"));
        assertFalse(secondTask.isStringValid("(("));
    }
}
