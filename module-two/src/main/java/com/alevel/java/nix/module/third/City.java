package com.alevel.java.nix.module.third;

import java.util.*;

public final class City {
    final private int id;
    final private Map<Integer, Integer> neighbors;

    public City(int id, Map<Integer, Integer> neighbors) {
        this.id = id;
        this.neighbors = neighbors;
    }

    public int findMinPath(City[] cities, int destination) {
        return findMinPath(cities, new HashSet<>(), destination, 0, Integer.MAX_VALUE);
    }

    private int findMinPath(City[] cities, Set<Integer> src, int destination, int currentDistance, int currentMin) {
        if (currentDistance >= 200_000 || currentMin < currentDistance) {
            return Integer.MAX_VALUE;
        }
        Set<Integer> allNeighbors = neighbors.keySet();
        int minPath = neighbors.getOrDefault(destination, Integer.MAX_VALUE - currentDistance) + currentDistance;
        src.add(id);
        for (int current : allNeighbors) {
            if (!src.contains(current)) {
                minPath = Math.min(minPath,
                        cities[current - 1].
                                findMinPath(cities, src, destination, currentDistance + neighbors.get(current), minPath));

            }
        }
        src.remove(id);
        return minPath;
    }

}
