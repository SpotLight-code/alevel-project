package com.alevel.java.nix.homework8.aggregation;

public abstract class BaseSubstance implements Substance {

    private double currentHeat = 20;
    private State s;

    abstract double getVigorTemperature();
    abstract double getGasTemperature();
    public abstract String getName();

    @Override
    public State heatUp(final double t) {

        currentHeat = t;

        if(currentHeat > getGasTemperature()){
            s = State.GAS;
        }else if(currentHeat > getVigorTemperature()){
            s = State.VIGOR;
        }else {
            s = State.SOLID;
        }

        return s;
    }

    @Override
    public double getTemperature() {
        return currentHeat;
    }

    @Override
    public State getState() {
        return s;
    }

    @Override
    public String toString(){
        return String.format("%s: current temperature = %.2f, current state = %s",
                getName(), getTemperature(), s.getState()) + System.lineSeparator();
    }
}
