create table accounts
(
    id      serial  not null
        constraint accounts_pkey
            primary key,
    balance integer not null,
    user_id integer
        constraint accounts_user_id_fkey
            references users
            on update cascade on delete cascade
);

alter table accounts
    owner to postgres;

INSERT INTO public.accounts (id, balance, user_id) VALUES (1, 500, 0);
INSERT INTO public.accounts (id, balance, user_id) VALUES (0, 200, 0);