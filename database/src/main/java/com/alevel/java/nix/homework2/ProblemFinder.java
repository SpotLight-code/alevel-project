package com.alevel.java.nix.homework2;

import com.alevel.java.nix.homework2.entity.City;
import com.alevel.java.nix.homework2.entity.Connection;
import com.alevel.java.nix.homework2.entity.Task;

import java.util.HashSet;
import java.util.Set;

public class ProblemFinder {

    private final City from;
    private final City to;

    public ProblemFinder(Task task){
        from = task.getFrom();
        to = task.getTo();
    }


    public int findMinPath() {
        return findMinPath(new HashSet<>(), to, 0, Integer.MAX_VALUE, from);
    }

    private int findMinPath(Set<Integer> src, City destination, int currentDistance, int currentMin, City currentCity) {
        if (currentDistance >= 200_000 || currentMin < currentDistance) {
            return Integer.MAX_VALUE;
        }

        int minPath = currentMin;

        src.add(currentCity.getId());
        for (Connection current : currentCity.getConnections()) {
            if (!src.contains(current.getToCity().getId())) {
                if (current.getToCity().getId().equals(destination.getId())) {
                    minPath = Math.min(minPath, currentDistance + current.getCost());
                    break;
                }
                minPath = Math.min(minPath,
                        findMinPath(src, destination, currentDistance + current.getCost(), minPath, current.getToCity()));

            }
        }
        src.remove(currentCity.getId());
        return minPath;
    }
}
