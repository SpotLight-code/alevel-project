package com.alevel.java.nix.practice1.gallows.launcher;

import com.alevel.java.nix.practice1.gallows.Gallows;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LauncherTest {

    @Test
    void launchFromRecourse() {
        assertDoesNotThrow(() -> {
            Launcher launcher = Launcher.launchFromRecourse("/forTest.txt");

            Gallows gallows = launcher.getGame();
            gallows.makeMove('h');
            gallows.makeMove('e');
            gallows.makeMove('l');
            gallows.makeMove('o');

            for (int i = 0; i < 5 && !gallows.isGameFinished(); i++) {
                assertEquals(5 - i, gallows.getChancesCount());
                gallows.makeMove(Character.forDigit(i, 10));
            }
            assertTrue(gallows.isGameFinished());

        });
    }
}