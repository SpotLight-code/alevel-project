package com.alevel.java.nix.module.third;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Integer.parseInt;

public class CityWizard {
    final private City[] cities;
    final private Map<String, Integer> cityMap;
    final private String[] task;

    private CityWizard(City[] cities, Map<String, Integer> cityMap, String[] task) {
        this.cities = cities;
        this.cityMap = cityMap;
        this.task = task;
    }

    public static CityWizard init(String path) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            CityWizard cityWizard;
            City[] cities = new City[parseInt(bufferedReader.readLine())];
            Map<String, Integer> map = new HashMap<>();
            for (int i = 0; i < cities.length; i++) {
                String name = bufferedReader.readLine();
                int count = parseInt(bufferedReader.readLine());
                Map<Integer, Integer> neighbours = new HashMap<>();
                for (int j = 0; j < count; j++) {
                    String[] values = bufferedReader.readLine().split(" ");
                    neighbours.put(parseInt(values[0]), parseInt(values[1]));
                }
                cities[i] = new City(i + 1, neighbours);
                map.put(name, i + 1);
            }
            String[] strings = new String[parseInt(bufferedReader.readLine())];
            for (int i = 0; i < strings.length; i++) {
                strings[i] = bufferedReader.readLine();
            }

            cityWizard = new CityWizard(cities, map, strings);

            return cityWizard;
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public int[] findAllTasks() {
        int[] res = new int[task.length];
        for (int i = 0; i < res.length; i++) {
            res[i] = findTask(i);
        }
        return res;
    }

    public int findTask(int index) {
        String[] data = task[index].split(" ");
        return cities[cityMap.get(data[0]) - 1].findMinPath(cities, cityMap.get(data[1]));
    }
}
