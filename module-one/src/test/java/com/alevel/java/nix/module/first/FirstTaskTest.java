package com.alevel.java.nix.module.first;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class FirstTaskTest {
    FirstTask firstTask = new FirstTask();


    @Test
    void countUniqueElements() {
        assertEquals(3, firstTask.countUniqueElements(1, 1, 2, 3));
        assertEquals(0, firstTask.countUniqueElements());
        assertEquals(5, firstTask.countUniqueElements(1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 1));
        assertEquals(2, firstTask.countUniqueElements(0, 3));
        assertEquals(1, firstTask.countUniqueElements(0, 0, 0, 0));
    }

    @Test
    void horseMove() {
        int[] startPosition = {3, 3};

        assertTrue(firstTask.canHorseMove(startPosition, new int[]{4, 5}));
        assertTrue(firstTask.canHorseMove(startPosition, new int[]{2, 5}));
        assertTrue(firstTask.canHorseMove(startPosition, new int[]{1, 4}));

        assertFalse(firstTask.canHorseMove(startPosition, new int[]{4, 4}));
        assertFalse(firstTask.canHorseMove(startPosition, new int[]{4, 3}));
        assertFalse(firstTask.canHorseMove(startPosition, new int[]{5, 3}));
    }

    @Test
    void triangleTest() {
        int[] a = {1, 5};
        int[] b = {6, -4};
        int[] c = {-2, 1};

        assertEquals(23.5, firstTask.triangleSquare(a, b, c), 0.001);

        a = new int[]{5, 5};
        b = new int[]{5, -1};
        c = new int[]{4, 2};

        assertEquals(3, firstTask.triangleSquare(a, b, c));
    }

}