package com.alevel.java.nix.module.second;

import java.util.*;

public class SecondTask {

    public static String findFirstUnique(List<String> str) throws Exception {
        Map<String, Integer> map = new HashMap<>();
        Set<String> resultSet = new LinkedHashSet<>();

        for (String s : str) {
            int res = map.merge(s, 1, Integer::sum);
            if (res == 1) {
                resultSet.add(s);
            } else if (res == 2) {
                resultSet.remove(s);
            }
        }

        return resultSet.stream().findFirst().orElseThrow(Exception::new);
    }
}
