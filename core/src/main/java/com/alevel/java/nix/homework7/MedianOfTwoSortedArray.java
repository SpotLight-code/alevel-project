package com.alevel.java.nix.homework7;


public class MedianOfTwoSortedArray {

    public double findMedianSortedArrays(int[] nums1, int[] nums2) { //runtime 2 ms O((n+m)/2).
        int maxLen = nums1.length + nums2.length;
        int last = 0, beforeLast = 0;

        int ii = 0;
        int jj = 0;

        for (int i = 0; i < maxLen / 2 + 1; i++) {
            beforeLast = last;
            if (ii < nums1.length) {
                if (jj < nums2.length) {
                    if (nums1[ii] < nums2[jj]) {
                        last = nums1[ii++];
                    } else {
                        last = nums2[jj++];
                    }
                } else {
                    last = nums1[ii++];
                }
                continue;
            }
            last = nums2[jj++];
        }
        if (maxLen % 2 == 1) {
            return last;
        } else {
            return (last + beforeLast) / 2.0;
        }
    }
}

