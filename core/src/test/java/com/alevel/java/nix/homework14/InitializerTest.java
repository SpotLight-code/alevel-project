package com.alevel.java.nix.homework14;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class InitializerTest {

    @Test
    void create() {
        AppProperties test = Initializer.create(AppProperties.class);
        assertEquals(10, test.getTest());
    }
}