package com.alevel.java.nix.homework3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TransposeTest {

    Transpose transpose = new Transpose();

    @Test
    void transposeMatrix() {

        int[][][] testArray = {
                {
                        {0, 1, 2},
                        {3, 4, 5},
                        {6, 7, 8}
                },
                {
                        {2, 3, 4},
                        {5, 6, 7}
                },
                {
                        {1, 2, 3}
                },
                {
                        {1, 2},
                        {3, 4},
                        {5, 6}
                }
        };

        int[][][] expected = {
                {
                        {0, 3, 6},
                        {1, 4, 7},
                        {2, 5, 8}
                },
                {
                        {2, 5},
                        {3, 6},
                        {4, 7}
                },
                {
                        {1},
                        {2},
                        {3}
                },
                {
                        {1, 3, 5},
                        {2, 4, 6}
                }
        };

        for (int i = 0; i < testArray.length; i++) {
            assertArrays(testArray[i], expected[i]);
        }

    }

    void assertArrays(int[][] expected, int[][] testArray){
        transpose.transposeMatrix(testArray);
        int[][] actual = transpose.getTransposedMatrix();
        for (int i = 0; i < expected.length; i++) {
            assertArrayEquals(expected[i], actual[i]);
        }
    }
}