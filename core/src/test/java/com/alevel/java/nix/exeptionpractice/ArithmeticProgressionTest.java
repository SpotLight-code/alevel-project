package com.alevel.java.nix.exeptionpractice;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ArithmeticProgressionTest {

    @Test
    void calculate() {

        assertDoesNotThrow(() -> {
            ArithmeticProgression arithmeticProgression = new ArithmeticProgression(0, 2);
            assertEquals(200, arithmeticProgression.calculate(100));
        });

        assertThrows(ProgressionConfigurationException.class,
                () -> new ArithmeticProgression(0, 0));

        assertThrows(ProgressionConfigurationException.class,
                () -> new ArithmeticProgression(0, 2).calculate(-100));

    }
}