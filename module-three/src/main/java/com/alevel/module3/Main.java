package com.alevel.module3;

import com.alevel.module3.firstmode.UiLoop;
import com.alevel.module3.secondmode.ReportGenerator;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class Main {
    public static void main(String[] args) {

        switch (args.length) {
            case 3:
                new UiLoop().begin(args[0], args[1], args[2]);
                break;
            case 4:
                launch(args, false);
                break;
            case 6:
                launch(args, true);
                break;
            default:
                StringBuilder stringBuilder = new StringBuilder()
                        .append("Invalid count of arguments, it must be 3, 4 or 6").append(System.lineSeparator()).append(System.lineSeparator())
                        .append("For 3 params application will be in creation operations mode, where").append(System.lineSeparator())
                        .append("\t1 param - user name").append(System.lineSeparator())
                        .append("\t2 param - password").append(System.lineSeparator())
                        .append("\t3 param - email of user").append(System.lineSeparator()).append(System.lineSeparator())
                        .append("For 4 params application will generate report about operations for all period, where").append(System.lineSeparator())
                        .append("\t1,2,3 params like creation operations mode").append(System.lineSeparator())
                        .append("\t4 param - path to csv file in which this file will be created, path must included file name and .csv").append(System.lineSeparator()).append(System.lineSeparator())
                        .append("For 6 params application will generate report about operations for custom period of time, where").append(System.lineSeparator())
                        .append("\t1,2,3 params like creation operations mode").append(System.lineSeparator())
                        .append("\t4,5 params - date between which will be selected operations").append(System.lineSeparator())
                        .append("\t6 param - path to csv file in which this file will be created, path must included file name and .csv");
                System.out.println(stringBuilder.toString());


        }

    }

    private static Instant tryToGetInstant(String str) throws IllegalAccessException {
        try {
            return LocalDateTime.parse(str,
                    DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss")).toInstant(ZoneOffset.UTC);
        } catch (DateTimeParseException e) {
            try {
                return LocalDate.parse(str,
                        DateTimeFormatter.ofPattern("yyyy-MM-dd")).atStartOfDay().toInstant(ZoneOffset.UTC);
            } catch (DateTimeParseException e1) {
                throw new IllegalAccessException(
                        "Unsupported date format, supported pattern: [yyyy-MM-dd hh:mm:ss, yyyy-MM-dd]. Input " + str);
            }
        }
    }

    private static void launch(String[] args, boolean withDateParams) {
        if (withDateParams) {
            try {
                new ReportGenerator(args[0], args[1], args[2], tryToGetInstant(args[3]), tryToGetInstant(args[4]), args[5]);
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e.getMessage());
            }

        } else {
            new ReportGenerator(args[0], args[1], args[2], Instant.MIN, Instant.MAX, args[3]).generateCSV();
        }
    }
}
