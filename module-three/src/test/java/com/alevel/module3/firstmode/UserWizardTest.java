package com.alevel.module3.firstmode;

import com.alevel.module3.firstmode.entity.Operation;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class UserWizardTest {

    @Test
    void connectingToDatabase() {
        assertEquals("Incorrect username or password", assertThrows(RuntimeException.class,
                () -> new UserWizard("wrong user", "password", "mymail@gmail.com")).getMessage());
        assertEquals("Incorrect email", assertThrows(RuntimeException.class,
                () -> new UserWizard("postgres", "password", "wrongmail@gmail.com")).getMessage());

        assertDoesNotThrow(() -> new UserWizard("postgres", "password", "mymail@gmail.com").close());
    }

    @Test
    void newOperation() {

        try (UserWizard userWizard = new UserWizard("postgres", "password", "mymail@gmail.com")) {
            assertTrue(userWizard.getAccounts().size() > 1);

            assertTrue(userWizard.getUnconfirmedOperations().isEmpty());
            userWizard.newOperation(0, 10, "test");

            assertFalse(userWizard.getUnconfirmedOperations().isEmpty());
            assertEquals(1, userWizard.getUnconfirmedOperations().size());

            assertDoesNotThrow(() -> userWizard.confirmOperation(0));
            assertTrue(userWizard.getUnconfirmedOperations().isEmpty());

            List<Operation> operationList = userWizard.getAccounts().get(0).getOperations();

            assertFalse(operationList.isEmpty());

            userWizard.getAccounts().get(0).getOperations().remove(operationList.size() - 1);

            userWizard.save();

            List<Operation> operationListWithOutTestValue = userWizard.getAccounts().get(0).getOperations();
            if (!operationListWithOutTestValue.isEmpty()
                    && !operationListWithOutTestValue.get(operationListWithOutTestValue.size() - 1).getIncomes().isEmpty()) {


                assertNotEquals("test",
                        operationListWithOutTestValue.get(operationListWithOutTestValue.size() - 1).getIncomes().get(0).getInform());
            }
        }
    }

    @Test
    void cancelOperation() {

        try (UserWizard userWizard = new UserWizard("postgres", "password", "mymail@gmail.com")) {

            int size = userWizard.getAccounts().get(0).getOperations().size();

            assertTrue(userWizard.getUnconfirmedOperations().isEmpty());
            userWizard.newOperation(0, 10, "test");

            assertDoesNotThrow(userWizard::reload);

            Assertions.assertEquals(size, userWizard.getAccounts().get(0).getOperations().size());

            assertFalse(userWizard.getUnconfirmedOperations().isEmpty());

            userWizard.cancelOperation(0);

            assertTrue(userWizard.getUnconfirmedOperations().isEmpty());

            assertDoesNotThrow(userWizard::reload);

            Assertions.assertEquals(size, userWizard.getAccounts().get(0).getOperations().size());
        }
    }

}