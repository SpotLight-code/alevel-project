package com.alevel.java.nix.homework14;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Properties;

public class Initializer {
    public static <T> T create(Class<T> type) {
        try (var input = AppProperties.class.getClassLoader().getResourceAsStream("app.properties")) {
            T t = type.getConstructor().newInstance();
            Properties properties = new Properties();
            properties.load(input);
            for (var field : type.getDeclaredFields()) {
                if (field.isAnnotationPresent(PropertyKey.class)) {
                    String data = properties.getProperty(field.getDeclaredAnnotation(PropertyKey.class).key());
                    switch (field.getAnnotatedType().toString()) {
                        case "int": {
                            field.setInt(t, Integer.parseInt(data));
                            break;
                        }
                        case "byte": {
                            field.setByte(t, Byte.parseByte(data));
                            break;
                        }
                        case "boolean": {
                            field.setBoolean(t, Boolean.parseBoolean(data));
                            break;
                        }

                        case "float": {
                            field.setFloat(t, Float.parseFloat(data));
                            break;
                        }
                        case "double": {
                            field.setDouble(t, Double.parseDouble(data));
                            break;
                        }
                        case "long": {
                            field.setLong(t, Long.parseLong(data));
                            break;
                        }
                        case "char": {
                            field.setChar(t, data.charAt(0));
                            break;
                        }
                        default: {
                            field.set(t, data);
                        }
                    }

                }
            }
            return t;
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException | IOException e) {
            throw new RuntimeException(e);
        }
    }
}
