package com.alevel.java.nix.module.first;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class FirstTaskTest {

    @Test
    void reformatDate() {
        String[] forTest = {"2020/04/05", "05/04/2020", "04-05-2020", "2020-05-04"};
        String expected = "20200405";
        List<String> actual = FirstTask.reformatDate(Arrays.asList(forTest));
        assertEquals(3, actual.size());
        for (String s : actual) {
            assertEquals(s, expected);
        }
    }
}