package com.alevel.java.nix.homework4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WaterContainerTest {

    @Test
    void maxArea() {
        WaterContainer waterContainer = new WaterContainer();

        assertEquals(49,waterContainer.maxArea(1,8,6,2,5,4,8,3,7));
        assertEquals(4,waterContainer.maxArea(1,2,4,3));
        assertEquals(17,waterContainer.maxArea(2,3,4,5,18,17,6));
        assertEquals(54,waterContainer.maxArea(5,6,3,2,6,7,5,3,5,7,9));
        assertEquals(20,waterContainer.maxArea(3,4,6,9,6,5,4));
        assertEquals(24,waterContainer.maxArea(4,4,5,6,6,6,6));
        assertEquals(12,waterContainer.maxArea(1,2,3,4,5,6,7));
        assertEquals(21,waterContainer.maxArea(3,4,4,3,3,4,4,3));

    }
}