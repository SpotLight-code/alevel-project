package com.alevel.java.nix.homework4;

public class WaterContainer {

    public int maxArea(int... height) {

        int length = height.length;
        int result = 0;
        int i = 0, j = length - 1;

        while (i != j) {
            int maxL = height[i];
            int maxR = height[j];

            int temp = (j - i) * Math.min(maxL, maxR);

            if (temp > result) {
                result = temp;
            }

            if (height[i] < height[j]) {
                i++;
            } else {
                j--;
            }


        }
        return result;
    }
}
