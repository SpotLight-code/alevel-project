package com.alevel.java.nix.hippodrome;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

import static com.alevel.java.nix.hippodrome.Constants.*;

public class Horse implements Runnable, Comparable<Horse>{
    private Duration timeRiding;
    private final  CountDownLatch latch;
    private final int horseNumber;
    private int currentDistance = 0;

    private final CyclicBarrier barrier;

    public Horse(int horseNumber, CyclicBarrier barrier, CountDownLatch latch) {
        this.latch = latch;
        this.barrier = barrier;
        this.horseNumber = horseNumber;
    }
    @Override
    public void run() {
        try {
            barrier.await();
            LocalDateTime start = LocalDateTime.now();
            Random random = new Random();
            while (currentDistance < DISTANCE){
                currentDistance += random.nextInt(MAX_DISTANCE - MIN_DISTANCE) + MIN_DISTANCE;
                Thread.sleep(random.nextInt(MAX_SLEEP - MIN_SLEEP) + MIN_SLEEP);
            }
            timeRiding = Duration.between(start, LocalDateTime.now());
        } catch (InterruptedException | BrokenBarrierException e) {
            e.printStackTrace();
        }finally {
            latch.countDown();
            barrier.reset();
        }
    }

    public Duration getTimeRiding() {
        return timeRiding;
    }

    public boolean isNotDisqualified(){
         return DISTANCE <= currentDistance;
    }

    public int getHorseNumber() {
        return horseNumber;
    }

    @Override
    public int compareTo(Horse horse) {
        return timeRiding.compareTo(horse.timeRiding);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Horse horse = (Horse) o;
        return horseNumber == horse.horseNumber;
    }

    @Override
    public int hashCode() {
        return Objects.hash(horseNumber);
    }

    @Override
    public String toString() {
        return MessageFormat.format("horse({0}), duration riding = {1}", horseNumber, timeRiding.toMillis());
    }
}
