package com.alevel.java.nix.homework13;

import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.junit.jupiter.api.Assertions.*;

class StructTest {

    @Test
    void load() {
        Struct struct = Struct.load("file.csv");

        assertDoesNotThrow(() -> {
            for (int i = 0; i < 5; i++) {
                for (int j = 0; j < 5; j++) {
                    assertEquals(i * 5 + j + 1, Integer.parseInt(struct.getValue(i, j)));
                }
            }
        });

        assertDoesNotThrow(() -> assertEquals(String.valueOf(1), struct.getValue(0, "t1")));
        assertThrows(NoSuchElementException.class, () -> struct.getValue(0, "illegal"));
        assertEquals("[t1, t2, t3, t4, t5]", struct.headerList().toString());
    }
}