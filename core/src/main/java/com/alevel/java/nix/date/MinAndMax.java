package com.alevel.java.nix.date;

public final class MinAndMax<T extends Comparable<? super T>> {
    final private T min;
    final private T max;

    private MinAndMax(T min, T max) {
        this.min = min;
        this.max = max;
    }

    public static <R extends Comparable<? super R>>MinAndMax<R> init(R elem) {
        return new MinAndMax<>(elem, elem);
    }

    public static <R extends Comparable<? super R>> MinAndMax<R> setUpValues(MinAndMax<R> elem1, MinAndMax<R> elem2){
        R min = elem1.min.compareTo(elem2.min) < 0 ? elem1.min : elem2.min;
        R max = elem1.max.compareTo(elem2.max) > 0 ? elem1.max : elem2.max;
        return new MinAndMax<>(min,max);
    }

    public T getMin() {
        return min;
    }

    public T getMax() {
        return max;
    }
}
