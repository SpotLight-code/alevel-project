package com.alevel.java.nix.homework8.aggregation;

public enum State {
    VIGOR("vigor"),
    SOLID("solid"),
    GAS("gas");

    public String getState() {
        return state;
    }

    final private String state;

    State(String state) {
        this.state = state;
    }
}
